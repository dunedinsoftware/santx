import java.io.IOException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;
import java.util.List;
import java.util.Arrays;
import java.util.stream.Collectors;

public class MatrizInv {

	public static void main(String args[]) {

		final Stream<String> lines;

        try {

        
        	OutputStream out2 = new FileOutputStream(args[0] + "inv.txt");
        	PrintStream out = new PrintStream(out2);

            lines = Files.lines(Paths.get(args[0]));
            String[][] matriz = lines.map(line -> line.split(";")).toArray(String[][]::new);


            for (int j=0; j < matriz[0].length; j++){
				for (int i=0; i < matriz.length; i++){
					if (i == matriz.length - 1)
            			out.printf("%s", matriz[i][j].replace(",", "."));
            		else
            			out.printf("%s;", matriz[i][j].replace(",", "."));
            	}
            	out.println();
            }

            out.close();


        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}