package com.dunedin.sensorx.analise.controller;

import com.dunedin.sensorx.analise.service.UserService;

import com.dunedin.sensorx.analise.model.User;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

//import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/")
public class MainController {

	//protected static Logger logger = Logger.getLogger("controller");
	@Autowired
	private UserService usuarioService;
        
        @RequestMapping(value="/", method = RequestMethod.GET)
        public String showLogonForm(HttpServletRequest request, HttpServletResponse response)
        {
            return "index";
        }
        
        @RequestMapping(value="/autenticar", method=RequestMethod.POST)
	public String autenticar(
                @RequestParam(name="username", required=true)  String username,
                @RequestParam(name="password", required=true)  String password,
		HttpServletResponse response,
		HttpSession session){
            
                
		User usuario = null;

		try
		{
			usuario = usuarioService.findUser(username, password);

			if (usuario != null)
			{

				if (usuario.getId() > 0){

					session.setAttribute("usuario", usuario);
					return "dashboard";

				}
				else
				{
					return "redirect:/";
				}

			}
			else
			{
				return "redirect:/";
			}

		}
		catch(Exception e)
		{
			e.printStackTrace();
			System.out.println("Usuario:" + username);
			return "redirect:/";
		}
                

	}

}