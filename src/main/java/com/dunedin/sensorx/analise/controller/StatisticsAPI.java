package com.dunedin.sensorx.analise.controller;

//import com.dunedin.graficos.*;

import com.dunedin.sensorx.analise.core.*;
import com.dunedin.sensorx.analise.sensorial.*;
import com.dunedin.sensorx.analise.utils.Dummy;
import com.dunedin.sensorx.analise.utils.Constantes;
import com.dunedin.sensorx.analise.utils.Formatacao;
import com.dunedin.sensorx.analise.utils.Grafico;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.IOException;
import java.io.Serializable;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import java.text.DateFormat;
import java.text.MessageFormat;
import java.text.NumberFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.Random;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Scanner;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;

import org.jfree.chart.ChartFactory;

import org.jfree.chart.ChartUtils; //versao 1.6
//import org.jfree.chart.ChartUtilities; //versao 1.0.19
import org.jfree.chart.JFreeChart;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.general.DatasetUtils; 

import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.chart.renderer.xy.XYShapeRenderer;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.IntervalXYDataset;

import org.jfree.data.statistics.DefaultBoxAndWhiskerCategoryDataset;
import org.jfree.data.statistics.BoxAndWhiskerCategoryDataset;
import org.jfree.data.statistics.BoxAndWhiskerXYDataset;
import org.jfree.data.statistics.BoxAndWhiskerItem;
import org.jfree.data.statistics.HistogramDataset;

/*
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.annotation.Autowired;
*/
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.request.WebRequest;
//import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.multipart.support.StandardServletMultipartResolver;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@RequestMapping("/stat")
public class StatisticsAPI implements Constantes{

	private static final String EXTENSAO_ARQUIVO = ".txt";
	private static final String PROMPT = ">&nbsp;";
	
        private static int CASAS_DECIMAIS_USUARIO = 2;

	public static String br(int n){
		String ret = "";
		for (int i = 0; i < n; i++) ret += "</br>";
		return ret;
		//return "\n";
	}

	public static String espaco(int n){
		String ret = "";
		for (int i = 0; i < n; i++) ret += "-";
		return ret;
	}

	public static String hifen(int n){
		String ret = "";
		for (int i = 0; i < n; i++) ret += "-";
		return ret;
	}

	

	public static String formatar(double x){

		//System.out.println(x);
		double dec = 0;
		String ret = x + "";

		try {

			String antes = ret.substring(0, ret.indexOf("."));
			String apos  = ret.substring(ret.indexOf(".") + 1);

			if (apos.length() <= CASAS_DECIMAIS_USUARIO) return ret.replace(".", ",");

			dec = Double.parseDouble(apos.substring(0, CASAS_DECIMAIS_USUARIO) + "." + apos.substring(CASAS_DECIMAIS_USUARIO));
			ret = antes + "," + Math.round(dec);

		}
		catch(Exception e){
			e.getMessage();
		}

		return ret;

	}

	public static String formatar(double x, int casas){

		//System.out.println(x);
		double dec = 0;
		String ret = x + "";

		try {

			String antes = ret.substring(0, ret.indexOf("."));
			String apos  = ret.substring(ret.indexOf(".") + 1);

			if (apos.length() <= CASAS_DECIMAIS_USUARIO) return ret.replace(".", ",");

			dec = Double.parseDouble(apos.substring(0, casas) + "." + apos.substring(casas));
			ret = antes + "," + Math.round(dec);
		}
		catch(Exception e){
			e.getMessage();
		}

		return ret;

	}

	public static String tabular(String x, int largCol){
		String str = "<span style='color:white'>";
		str += espaco(largCol - x.length());
		str += "</span><span style='color:black'>" + x + "</span>";
		return str ;
	}

	public static String td(String x){
		return "<td>" + x + "</td>";
	}

	public static String abreTr(){
		return "<tr>";
	}

	public static String fechaTr(){
		return "</tr>";
	}

	
	public String[][] getArquivo(String nomeArq){

		String amostras[][] = null;
		String linha = "";
		String cols[];
		ArrayList<String> als = null;
		ArrayList<ArrayList<String>> lam = new ArrayList<ArrayList<String>>();

		try {

			File f = new File(AMOSTRAS_PATH + nomeArq + EXTENSAO_ARQUIVO);
			BufferedReader buffR = new BufferedReader(new InputStreamReader(new FileInputStream(f)));

			while ((linha = buffR.readLine()) != null){

				als = new ArrayList<String>();
				cols = linha.split(";");
				for (int j=0; j < cols.length; j++){
					als.add(cols[j]);
				}
				lam.add(als);
				
			}

			amostras = new String[lam.size()][als.size()];

			for (int i=0; i < lam.size(); i++)
				for (int j=0; j < als.size(); j++)
					amostras[i][j] = lam.get(i).get(j);
		
		}
		catch(Exception e){

		}

		return amostras;

	}

	@RequestMapping(value="/arquivos/{nomeArq}", method = RequestMethod.GET)
	public void showArquivo(@PathVariable("nomeArq") String nomeArq, HttpServletRequest request, HttpServletResponse response, Model model) throws ServletException, IOException{

		String amostras[][] = getArquivo(nomeArq);
		ServletOutputStream out = null; 
		String ret = "";
		ret += "[";
		
		for (int i=0; i < amostras.length;i++){
			ret += "{";
			ret += "\"fator\":\"" + amostras[i][0] + "\",";
			ret += "\"notas\":";
				
			ret += "[";	
			for (int j=1; j < amostras[i].length; j++){
				ret += amostras[i][j] + ",";
			}
			ret = ret.substring(0, ret.length() - 1);
			ret += "]";
 			ret += "},";
 		}	

		ret = ret.substring(0, ret.length() - 1);
		ret += "]";

		
        response.setContentType("application/json");
        response.setHeader("Access-Control-Allow-Origin", "*");
        out = response.getOutputStream();
        out.println(ret);
        out.close();

		/*
		model.addAttribute("ret", ret);
		return "index";
		*/
		
		/*
	  	HttpServletResponse builder = Response.ok()
	 				  .entity(new GenericEntity<String>(ret){})
					  .header("Access-Control-Allow-Origin", "*");
		return builder.build();
		*/

	}

	@RequestMapping(value="/arquivos", method = RequestMethod.GET)
	public void listarArquivos(HttpServletRequest request, HttpServletResponse response, Model model) throws ServletException, IOException{

		ServletOutputStream out = null;

        Object arrObj[] = Files.list((new File(AMOSTRAS_PATH)).toPath()).toArray();

        String ret = "";
		ret += "[{";
		ret += "\"arquivos\":";
		ret += "[";	
		for (int i=0; i < arrObj.length; i++){

			Path path = (Path) arrObj[i];
			String nomeArq = path.getFileName().toString();
			nomeArq = nomeArq.substring(0, nomeArq.indexOf("."));
			ret += "\"" + nomeArq + "\",";

		}
		ret = ret.substring(0, ret.length() - 1);
		ret += "]";
		ret += "}]";

		response.setContentType("application/json");
        response.setHeader("Access-Control-Allow-Origin", "*");
        out = response.getOutputStream();
        out.println(ret);
        out.close();

	}

	@RequestMapping(value="/anova-two-way", method = RequestMethod.GET)
	public void showTwoWayAnova(@RequestParam("nomeArqCT") String nomeArqCT,
								@RequestParam("nomeArqST") String nomeArqST,
								HttpServletRequest request, 
								HttpServletResponse response, Model model) throws ServletException, IOException{

		ServletOutputStream out = null; 
		String ret = "";
		
		AnovaTwoWay anova = new AnovaTwoWay();
		anova.setAmostrasCT(getArquivo(nomeArqCT));
		anova.setAmostrasST(getArquivo(nomeArqST));
		TabelaDistribuicao095f tabF = new TabelaDistribuicao095f();

		/*
		Na versao 1.0.19, precisa corrigr a chamada deste metodo
		String nomeImg = gerarGraficoMediasAnovaTwoWay(new double[][]{{anova.getMediaST(0), anova.getMediaST(1)},
													 {anova.getMediaCT(0), anova.getMediaCT(1)}
		});
		*/
		
		response.setContentType("text/html");
        response.setHeader("Access-Control-Allow-Origin", "*");
        out = response.getOutputStream();

        /* 
        out.println(PROMPT + nomeArqST + "/" + nomeArqCT + br(2));
        out.println("<div><img src='" + GRAFICOS_PATH_VIRTUAL + nomeImg + "' width=600 height=600 style=z-index:0'/></div>");

        out.println(tabular("Media (A1, B1) = " + formatar(anova.getMediaST(0)), 30));
        out.println(tabular("Media (A1, B2) = " + formatar(anova.getMediaCT(0)), 30));
        out.println(tabular("Media (A2, B1) = " + formatar(anova.getMediaST(1)), 30));
        out.println(tabular("Media (A2, B2) = " + formatar(anova.getMediaCT(1)), 30));
        out.println(br(2));
		*/

        out.println("RESUMO DA ANOVA 2 FATORES" + br(1));
		out.println(tabular("SS", 60)   + tabular("DF", 30) + tabular("MS", 30) + tabular("F", 30) + br(1));
		out.println(hifen(165) + br(1));
		out.println(tabular("TOTAL", 30) + tabular(formatar(anova.getSS()), 30) + tabular((anova.getQtdeTotalObs() - 1) + "", 30) + br(1));
		
		out.println(tabular("CELULAS", 30)     + tabular(formatar(anova.getCelulas_SS()), 30) 			 + tabular(anova.getCelulas_DF() + "", 30) + br(1));
		out.println(tabular("FATOR(A)", 30)     + tabular(formatar(anova.getFatorA_SS()), 30) 			 + tabular(anova.getFatorA_DF() + "", 30) 			  + tabular(formatar(anova.getFatorA_MS()), 30) 			+ tabular(formatar(anova.getFatorA_F()), 30) + br(1));
		out.println(tabular("FATOR(B)", 30)     + tabular(formatar(anova.getFatorB_SS()), 30) 			 + tabular(anova.getFatorB_DF() + "", 30) 			  + tabular(formatar(anova.getFatorB_MS()), 30) 			+ tabular(formatar(anova.getFatorB_F()), 30) + br(1));
		out.println(tabular("INTERACAO", 30)   + tabular(formatar(anova.getInteracaoFatoresAB_SS()), 30) + tabular(anova.getInteracaoFatoresAB_DF() + "", 30) + tabular(formatar(anova.getInteracaoFatoresAB_MS()), 30) + tabular(formatar(anova.getInteracaoFatoresAB_F()), 30) + br(1));
		out.println(tabular("ERRO", 30)        + tabular(formatar(anova.getErro_SS()), 30) 				 + tabular(anova.getErro_DF() + "", 30)				  + tabular(formatar(anova.getErro_MS()), 30) + br(1));
		
		out.println(hifen(165) + br(1));
		out.println("F tabela (5%) = " + formatar(tabF.getValorCritico(anova.getQtdeAmostrasCT() - 1, anova.getErro_DF())) + br(1));


        
        out.close();

	}

	@RequestMapping(value="/anova", method = RequestMethod.GET)
	public void showAnova(@RequestParam("nomeArq") String nomeArq, HttpServletRequest request, HttpServletResponse response, Model model) throws ServletException, IOException{

		ServletOutputStream out = null; 
		String ret = "";
		
		Anova anova = new Anova();
		anova.setAmostras(getArquivo(nomeArq));
		TabelaDistribuicao095f tabF = new TabelaDistribuicao095f();
		
		response.setContentType("text/html");
        response.setHeader("Access-Control-Allow-Origin", "*");
        out = response.getOutputStream();

        out.println(PROMPT + nomeArq + br(1));
        out.println("RESUMO DA ANOVA" + br(1));
		out.println(tabular("SS", 60) + tabular("DF", 30) + tabular("MS", 30) + br(1));
		out.println(hifen(165) + br(1));
		out.println(tabular("TOTAL", 30) + tabular(formatar(anova.getSS()), 30) + tabular((anova.getQtdeTotalObs() - 1) + "", 30) + br(1));
		out.println(tabular("ENTRE-AMOSTRAS", 24) + tabular(formatar(anova.getGruposSS()), 30) + tabular((anova.getQtdeAmostras() - 1) + "", 30) + tabular(formatar(anova.getGruposMS()), 30) + br(1));
		out.println(tabular("ERRO", 30) + tabular(formatar(anova.getErroSS()), 30) + tabular((anova.getQtdeTotalObs() - anova.getQtdeAmostras()) + "", 30) + tabular(formatar(anova.getErroMS()), 30) + br(1));
		out.println(hifen(165) + br(1));
		out.println("F calculado = " + formatar(anova.getFCalculado()) + br(1));
		out.println("F tabela (5%) = " + formatar(tabF.getValorCritico(anova.getQtdeAmostras() - 1, anova.getErroDF())) + br(1));

        out.close();

	}

	@RequestMapping(value="/histograma", method = RequestMethod.GET)
	public void showHistograma(@RequestParam("nomeArq") String nomeArq, HttpServletRequest request, HttpServletResponse response, Model model) throws ServletException, Exception{

		ServletOutputStream out = null; 

		String str = "";
		String nomeImg = "";
		String nomeImgBaw = "";
		double amostraDoubleSemLabel[] = null;
		
		Amostras amostras = new Amostras();
		amostras.setAmostras(getArquivo(nomeArq));

		int qtdeProvadores = amostras.getQtdeProvadores(0);
		int qtdeAmostras = amostras.getQtdeAmostras();

		String aNomeAmostra[] = new String[qtdeAmostras];
		double aMedia[] 	  = new double[qtdeAmostras]; 
		double aQ2[] 		  = new double[qtdeAmostras]; 
		double aQ1[]          = new double[qtdeAmostras]; 
		double aQ3[]          = new double[qtdeAmostras]; 
		double aLinf[]        = new double[qtdeAmostras]; 
		double aLsup[]        = new double[qtdeAmostras];	

		for (int i = 0; i < qtdeAmostras; i++){

			nomeImg    = Grafico.gerarHistograma(amostras.getNomeAmostra(i), amostras.getNotasPorAmostra(i));
			
			str += (PROMPT + nomeArq + br(1));
			str += ("<table><tr><td><img src='" + GRAFICOS_PATH_VIRTUAL + nomeImg + "' width=600 height=600 style=z-index:0'/><td></tr>");
			str += ("<tr><td>Media: " + formatar(amostras.getMedia(i), 2) + "</td><td></tr>");
			str += ("<tr><td>Var: " + formatar(amostras.getVariancia(i), 2) + "</td><td></tr>");
			str += ("<tr><td>DP: " + formatar(amostras.getDesvioPadrao(i), 2) + "</td><td></tr></table>");

			str += (br(2));	

			aNomeAmostra[i] = amostras.getNomeAmostra(i);
			aMedia[i] 		= amostras.getMedia(i); 
			aQ2[i] 			= amostras.getQ2(i); 
			aQ1[i]          = amostras.getQ1(i); 
			aQ3[i]          = amostras.getQ3(i); 
			aLinf[i]        = amostras.getQ1(i) - 1.5 * (amostras.getQ3(i) - amostras.getQ1(i)); 
			aLsup[i]        = amostras.getQ3(i) + 1.5 * (amostras.getQ3(i) - amostras.getQ1(i));		

	    }

	    nomeImgBaw = Grafico.gerarBoxAndWhisker(" ", aMedia, aQ2, aQ1, aQ3, aLinf, aLsup, 0, 0);


	    str += (PROMPT + nomeArq + br(1));
		str += ("<table><tr><td><img src='" + GRAFICOS_PATH_VIRTUAL + nomeImgBaw + "' width=600 height=600 style=z-index:0'/><td></tr></table>");
		str += (br(2));

		response.setContentType("text/html");
        response.setHeader("Access-Control-Allow-Origin", "*");
        out = response.getOutputStream();
		out.println(str);
        out.close();

	}

	/*
	@RequestMapping(value="/adq", method = RequestMethod.GET)
	public void showAdq(@RequestParam("nomeArq") String nomeArq, HttpServletRequest request, HttpServletResponse response, Model model) throws ServletException, IOException{

		ServletOutputStream out = null; 

		String str = "";
		String nomeImg = "";
		
		String amostras[][] = getArquivo(nomeArq);
		Adq adq = new Adq(amostras);
		int qtdeProvadores = adq.getQtdeProvadores(0);
		int qtdeAmostras = adq.getQtdeAmostras();

		
		TabelaDistribuicao095f tabF = new TabelaDistribuicao095f();




		for (int j = 1; j <= qtdeProvadores; j++){

			String indiv[][] = adq.getAmostras(j);

			Anova anova = new Anova();
			anova.setAmostras(indiv);
			
	        str += (">" + nomeArq + br(1));
			str += ("RESUMO DA ANOVA PROVADOR " + j + " vs PAINEL" + br(1));
			str += (tabular("SS", 60) + tabular("DF", 30) + tabular("MS", 30) + br(1));
			str += (hifen(165) + br(1));
			str += (tabular("TOTAL", 30) + tabular(formatar(anova.getSS()), 30) + tabular((anova.getQtdeTotalObs() - 1) + "", 30) + br(1));
			str += (tabular("ENTRE-AMOSTRAS", 24) + tabular(formatar(anova.getGruposSS()), 30) + tabular((anova.getQtdeAmostras() - 1) + "", 30) + tabular(formatar(anova.getGruposMS()), 30) + br(1));
			str += (tabular("ERRO", 30) + tabular(formatar(anova.getErroSS()), 30) + tabular((anova.getQtdeTotalObs() - anova.getQtdeAmostras()) + "", 30) + tabular(formatar(anova.getErroMS()), 30) + br(1));
			str += (hifen(165) + br(1));
			str += ("F calculado = " + formatar(anova.getFCalculado()) + br(1));
			str += ("F tabela (5%) = " + formatar(tabF.getValorCritico(anova.getQtdeAmostras() - 1, anova.getErroDF())) + br(2));

			for (int colX = 0; colX < qtdeAmostras - 1; colX++){

				for (int colY = colX + 1; colY < qtdeAmostras; colY++){

					 nomeImg = gerarGraficoMediasAdq("Provador " + j + " vs Painel",
					  amostras[colX][0], amostras[colY][0], 
					 	new double[][]{{
						Double.parseDouble(indiv[0][colX]),
						Double.parseDouble(indiv[1][colX])},{
						Double.parseDouble(indiv[0][colY]),
						Double.parseDouble(indiv[1][colY])}});

					str += (PROMPT + nomeArq + br(1));

					str += ("<table><tr><td><img src='" + GRAFICOS_PATH_VIRTUAL + nomeImg + "' width=600 height=600 style=z-index:0'/><td>");
					str += ("<table><tr><td></td><td colspan='2' align='center'>Medias</td></tr>");
					str += ("<tr><td></td><td>Provador " + j + "</td><td>Painel</td></tr>");

					str += ("<tr><td>" + amostras[colX][0] + "</td><td>" + formatar(Double.parseDouble(indiv[0][colX])) + "</td><td>" + formatar(Double.parseDouble(indiv[1][colX])) + "</td></tr>");
					str += ("<tr><td>" + amostras[colY][0] + "</td><td>" + formatar(Double.parseDouble(indiv[0][colY])) + "</td><td>" + formatar(Double.parseDouble(indiv[1][colY])) + "</td></tr>");

			        str += ("</table></td></tr></table>");
			        str += (br(2));

		    	}

		    }

		}



		response.setContentType("text/html");
        response.setHeader("Access-Control-Allow-Origin", "*");
        out = response.getOutputStream();
		out.println(str);
        out.close();

	}
	*/

	@RequestMapping(value="/anova-bca", method = RequestMethod.GET)
	public void showAnovaBca(@RequestParam("nomeArq") String nomeArq, HttpServletRequest request, HttpServletResponse response, Model model) throws ServletException, IOException{

		ServletOutputStream out = null; 
		String ret = "";
		
		AnovaBlocosCompletosAleatorios anova = new AnovaBlocosCompletosAleatorios(getArquivo(nomeArq));
		TabelaDistribuicao095f tabF = new TabelaDistribuicao095f();
		
		response.setContentType("text/html");
        response.setHeader("Access-Control-Allow-Origin", "*");
        out = response.getOutputStream();

        out.println(PROMPT + nomeArq + br(1));
		out.println("RESUMO DA ANOVA - BLOCOS COMPLETOS ALEATORIOS" + br(1));
		out.println(tabular("SS", 60) + tabular("DF", 30) + tabular("MS", 30) + tabular("F", 30) + br(1));
		out.println(hifen(165) + br(1));
		
		out.println(tabular("TOTAL", 30) + tabular(formatar(anova.getSS()), 30) + tabular((anova.getQtdeTotalObs() - 1) + "", 30) + br(1));
		out.println(tabular("FATORES", 30) + tabular(formatar(anova.getSSFator()), 30) + tabular((anova.getQtdeAmostras() - 1) + "", 30) + tabular(formatar(anova.getFatorMS()), 30) + tabular(formatar(anova.getFFatorCalculado()), 30) + br(1));
		out.println(tabular("BLOCOS", 29) +  tabular(formatar(anova.getSSBloco()), 30) +  tabular((anova.getQtdeProvadores(0) - 1) + "", 30) +  tabular(formatar(anova.getBlocoMS()), 30) +  tabular(formatar(anova.getFBlocoCalculado()), 30) + br(1));		
		out.println(tabular("ERRO", 30) +  tabular(formatar(anova.getErroSS()), 30) +  tabular(anova.getErroDF() + "", 30) +  tabular(formatar(anova.getErroMS()), 30) + br(1));
		
		out.println(hifen(165) + br(1));
		//System.out.println("F calculado = " + anova.getFCalculado());
		out.println("F tabela (5%) = " + formatar(tabF.getValorCritico(anova.getQtdeAmostras() - 1, anova.getErroDF())));


        out.close();

	}

	@RequestMapping(value="/tukey", method = RequestMethod.GET)
	public void showTukey(@RequestParam("nomeArq") String nomeArq, HttpServletRequest request, HttpServletResponse response, Model model) throws ServletException, IOException{

		//Locale.setDefault(Locale.US);

		ServletOutputStream out = null; 
		String ret = "";
		String msg = "";
		
		Tukey tk = new Tukey(getArquivo(nomeArq));
		TabelatStudent dts = new TabelatStudent();
		
		response.setContentType("text/html");
        response.setHeader("Access-Control-Allow-Origin", "*");
        out = response.getOutputStream();

        double medias[][] = tk.getMedias();
        double qTabelado = 0; 
        double qCalculado = 0;
		qTabelado = dts.getValorCritico(0.90, tk.getErroDF());
		
		out.println(PROMPT + nomeArq + br(1));
		out.println("<table width=100%>");
		out.println(abreTr() + "TUKEY" + fechaTr());
		out.println(abreTr() + td("Comp") + td("Diff Medias") +  td("SE") +  td("q calculado") + td("q tabelado") + td("Conclusao") + fechaTr());

		for (int i = 0; i < medias.length; i++){
			for (int j = 0; j < medias[0].length; j++){
				if (i != j) {

					qCalculado = tk.getDiffMedias(i, j) / tk.getSE();

					if (qCalculado > qTabelado){
						msg = "Rejeitar";
					}
					else {
						msg = "Aceitar";
					}		

					msg += " Hipotese Nula: Media" + (i + 1) + " = Media" + (j + 1);
						
					out.println(abreTr() + td(tk.getMap(i) + " vs " + tk.getMap(j)) + 
						td(formatar(medias[i][1]) + " - " + formatar(medias[j][1]) + " = " + formatar(tk.getDiffMedias(i, j))) +
						td(formatar(tk.getSE())) + 
						td(formatar(qCalculado)) + td(formatar(qTabelado)) + 
						td(msg) + fechaTr());
					
				}

			}

		}
		out.println("</table>");
        out.close();

	}

	@RequestMapping(value="/friedman", method = RequestMethod.GET)
	public void showFriedman(@RequestParam("nomeArq") String nomeArq, HttpServletRequest request, HttpServletResponse response, Model model) throws ServletException, IOException{

		//Locale.setDefault(Locale.US);

		ServletOutputStream out = null; 
		String ret = "";
		String msg = "";
		
		TesteFriedman tf = new TesteFriedman(getArquivo(nomeArq));
		TabelaChiQuadrado dcq = new TabelaChiQuadrado();
		
		response.setContentType("text/html");
        response.setHeader("Access-Control-Allow-Origin", "*");
        out = response.getOutputStream();

        out.println(PROMPT + nomeArq + br(1));
        out.println("ANALISE FRIEDMAN DE VARIANCIA POR RANKS" + br(1));
        out.println("Chi Residual Calculado = " + formatar(tf.getChiR()) + br(1));
        out.println("Chi Residual Tabelado (5%) = " + formatar(dcq.getValorCritico(0.05, tf.getQtdeAmostras() - 1)));
 		
        out.close();

	}

	@RequestMapping(value="/cochran", method = RequestMethod.GET)
	public void showCochran(@RequestParam("nomeArq") String nomeArq, HttpServletRequest request, HttpServletResponse response, Model model) throws ServletException, IOException{

		//Locale.setDefault(Locale.US);

		ServletOutputStream out = null; 
		String ret = "";
		String msg = "";
		
		Cochran tf = new Cochran(getArquivo(nomeArq));
		TabelaChiQuadrado dcq = new TabelaChiQuadrado();
		
		response.setContentType("text/html");
        response.setHeader("Access-Control-Allow-Origin", "*");
        out = response.getOutputStream();

        out.println(PROMPT + nomeArq + br(1));
        out.println("TESTE Q DE COCHRAN" + br(1));
        out.println("Q Calculado = " + formatar(tf.getQ()) + br(1));
        out.println("Chi Residual Tabelado (5%) = " + formatar(dcq.getValorCritico(0.05, tf.getQtdeAmostras() - 1)));
 		
        out.close();

	}

	@RequestMapping(value = "/upload", method = RequestMethod.POST)
	public void upload(@RequestParam("arquivo") MultipartFile file) throws ServletException, IOException{
		
		try {

			file.transferTo(new File(AMOSTRAS_PATH + file.getOriginalFilename()));
			
		}
		catch(Exception e){
			e.printStackTrace();
		}

	}

	@RequestMapping(value="/regressao", method = RequestMethod.GET)
	public void showRegressao(@RequestParam("nomeArq") String nomeArq,
						 HttpServletRequest request, 
						 HttpServletResponse response) throws ServletException, IOException{

		ServletOutputStream out = null; 
		String ret = "";
		int colX = 0; //<<<<<<<<<<<<<<<<<<<<<<<<<<<
		int colY = 1; //<<<<<<<<<<<<<<<<<<<<<<<<<<<

		String amostras[][] = getArquivo(nomeArq);
		
		Regressao anova = new Regressao(colX, colY);
		anova.setAmostras(amostras);
		
		TabelaDistribuicao095f tabF = new TabelaDistribuicao095f();

		
		String nomeGraf = Grafico.gerarRegressao(amostras, anova.getAlfa(), anova.getBeta(), 
								anova.getMinimo(amostras, colX),
								anova.getMaximo(amostras, colX));
		

		response.setContentType("text/html");
        response.setHeader("Access-Control-Allow-Origin", "*");
        out = response.getOutputStream();

        
        out.println(PROMPT + nomeArq + br(1));
        out.println("<div><img src='" + GRAFICOS_PATH_VIRTUAL + nomeGraf + "' width=600 height=600 style=z-index:0'/></div>");
		

        //out.println(PROMPT + nomeArq + br(1));
        out.println("Alfa = " + formatar(anova.getAlfa(), 3) + br(1));
        out.println("Beta = " + formatar(anova.getBeta(), 3) + br(1));
        out.println("r2 = " + formatar(anova.getCoefDeterminacao()) + br(1));

        /* 
		out.println(tabular("SS", 60) + tabular("DF", 30) + tabular("MS", 30) + br(1));
		out.println(hifen(165) + br(1));
		out.println(tabular("TOTAL", 30) + tabular(formatar(anova.getSS()), 30) + tabular((anova.getQtdeTotalObs() - 1) + "", 30) + br(1));
		out.println(tabular("ENTRE-AMOSTRAS", 24) + tabular(formatar(anova.getGruposSS()), 30) + tabular((anova.getQtdeAmostras() - 1) + "", 30) + tabular(formatar(anova.getGruposMS()), 30) + br(1));
		out.println(tabular("ERRO", 30) + tabular(formatar(anova.getErroSS()), 30) + tabular((anova.getQtdeTotalObs() - anova.getQtdeAmostras()) + "", 30) + tabular(formatar(anova.getErroMS()), 30) + br(1));
		out.println(hifen(165) + br(1));
		out.println("F calculado = " + formatar(anova.getFCalculado()) + br(1));
		out.println("F tabela (5%) = " + formatar(tabF.getValorCritico(anova.getQtdeAmostras() - 1, anova.getErroDF())) + br(1));
		*/

		out.close();

	}

	@RequestMapping(value="/correlacao", method = RequestMethod.GET)
	public void showCorrelacao(@RequestParam("nomeArq") String nomeArq,
						 HttpServletRequest request, 
						 HttpServletResponse response) throws ServletException, Exception{

		ServletOutputStream out = null; 
		Correlacao correlacao = null;

		String ret = "";

		MatrizCorrelacao am = new MatrizCorrelacao();

		String amostras[][] = getArquivo(nomeArq);
		am.setAmostras(amostras);

		int qtdeAmostras = am.getQtdeAmostras();
		int qtdeProvadores = am.getQtdeProvadores(0);
		double mc[][] = am.getMatrizCorrelacao();

		response.setContentType("text/html");
        response.setHeader("Access-Control-Allow-Origin", "*");
        out = response.getOutputStream();

        out.println(PROMPT + nomeArq + br(2));
        out.println("Matriz de Correlacao" + br(2));

        out.println(tabular(".", 20));
		for (int i = 0; i < qtdeAmostras; i++)
			out.println(tabular(am.getNomeAmostra(i), 20));
	
		out.println(br(1));
		for (int i = 0; i < qtdeAmostras; i++){	
			out.println(tabular(am.getNomeAmostra(i), 20));	
			for (int j = 0; j < i + 1; j++){
				out.println(tabular(formatar(mc[i][j]), 20));
			}
			out.println(br(1));
		}	

		Procrustes p = new Procrustes();

		amostras = getArquivo(nomeArq);
		p.setAmostras(amostras);

		
		double eigenValores[] = p.getEigenValores();
		double eigenVetores[][] = p.getEigenVetores();

		double scores[][] = p.getScores();
		double ev1 = p.getPesoCP(eigenValores, 0);
		double ev2 = p.getPesoCP(eigenValores, 1);

		if (ev1 < ev2){
			double max = ev2;
			ev2 = ev1;
			ev1 = max;			
		}

		/* PROCRUSTES v0.9 */
		
		String nomeGraf = Grafico.gerarScatterPlot(scores, "CP 1(" + ev1 + "%)", "CP 2(" + ev2 + "%)");
		out.println(br(1));
		out.println("<div><img src='" + GRAFICOS_PATH_VIRTUAL + nomeGraf + "' width=600 height=600 style=z-index:0'/></div>");
				
		out.close();

	}
        
        @RequestMapping(value="/k-means", method = RequestMethod.GET)
	public void showKMeans(@RequestParam("nomeArq") String nomeArq,
						 HttpServletRequest request, 
						 HttpServletResponse response) throws ServletException, IOException{

            ServletOutputStream out = null; 
            String ret = "";

            String amostras[][] = getArquivo(nomeArq);

            Kmeansplusplus kmpp = new Kmeansplusplus(amostras);
            
            String nomeGraf = Grafico.gerarKMeans(kmpp.getMedidasSemCentros(),
                                                    kmpp.getArrayKmedias(),
                                                    "X", "Y");
		
            response.setContentType("text/html");
            response.setHeader("Access-Control-Allow-Origin", "*");
            out = response.getOutputStream();

            out.println(PROMPT + nomeArq + br(1));
            out.println("<div><img src='" + GRAFICOS_PATH_VIRTUAL + nomeGraf + "' width=600 height=600 style=z-index:0'/></div>");
	
            out.close();

	}

}
