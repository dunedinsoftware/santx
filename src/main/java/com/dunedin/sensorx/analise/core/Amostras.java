package com.dunedin.sensorx.analise.core;

import com.dunedin.sensorx.analise.utils.Arranjo;
import java.util.Arrays;

public class Amostras{

	protected String amostras[][];

	public Amostras(){


	}

	public void setAmostras(String amostras[][]){
		this.amostras = amostras;
	}

	public int getQtdeProvadores(int i){

		return amostras[i].length - 1;

	}

	public int getQtdeAmostras(){

		return amostras.length;

	}

	public String getNota(int i, int j){

		return amostras[i][j];

	}

	public double getNotaDouble(int i, int j) throws Exception{

		return Double.parseDouble(getNota(i, j));

	}

	public double[] getNotasPorAmostra(int i) throws Exception{

		int qtdeProvadores = getQtdeProvadores(i);

		double amostraDoubleSemLabel[] = new double[qtdeProvadores];
		
			for (int j = 0; j < qtdeProvadores; j++)
				amostraDoubleSemLabel[j] = Double.parseDouble(getNota(i, j + 1));
			

		return amostraDoubleSemLabel;

	}


	public String getNomeAmostra(int i){

		return amostras[i][0];

	}

	public int getQtdeTotalObs(){

		int soma = 0;

		for (int i=0;i < amostras.length; i++)
			for (int j=1;j < amostras[0].length; j++)
				soma++;

		return soma;

	}

	public double getSomaTodasObs(){

		double soma = 0;

		try {

		for (int i=0;i < amostras.length; i++)
			for (int j=1;j < amostras[i].length; j++)
				soma += Double.parseDouble(amostras[i][j]);
		}
		catch(Exception e){

		}

		return soma;

	}

	public double getSoma(int i){

		double soma = 0;

		try {

		for (int j=1; j < amostras[i].length ; j++)
			soma += Double.parseDouble(amostras[i][j]);

		}
		catch(Exception ex){


		}

		return soma;	

	}

	public double getMedia(int i){

		return getSoma(i) / (double) getQtdeProvadores(i);
	
	}

	public double getQ1(int i) throws Exception{

		double[] arr = new double[getQtdeProvadores(i)];
		int idxQ2 = 0, idxInf = 0; //base zero
		double q1 = 0.0;

		arr = Arrays.copyOf(getNotasPorAmostra(i), getQtdeProvadores(i));

		arr = Arranjo.ordenar(arr);

		if ((arr.length % 2) == 0)
			idxQ2 = arr.length / 2;
		else
			idxQ2 = (int) Math.floor(arr.length / 2);

		if (((idxQ2 + 1) % 2) == 0){
			idxInf = (idxQ2 + 1) / 2 - 1;
			q1 = (arr[idxInf] + arr[idxInf + 1]) / 2;
		}
		else
			q1 = arr[(int) Math.floor((idxQ2 + 1) / 2)];

		return q1;
	
	}

	public double getQ2(int i) throws Exception{

		double[] arr = new double[getQtdeProvadores(i)];
		int idxQ2 = 0, idxInf = 0; //base zero
		double q2 = 0.0;

		arr = Arrays.copyOf(getNotasPorAmostra(i), getQtdeProvadores(i));

		arr = Arranjo.ordenar(arr);

		if ((arr.length % 2) == 0){
			idxInf = arr.length / 2 - 1;
			q2 = (arr[idxInf] + arr[idxInf + 1]) / 2;
		}
		else {
			idxQ2 = (int) Math.floor(arr.length / 2);
			q2 = arr[idxQ2];
		}

		return q2;
	
	}

	public double getQ3(int i) throws Exception{

		double[] arr = new double[getQtdeProvadores(i)];
		int idxQ2 = 0, idxInf = 0; //base zero
		double q3 = 0.0;

		arr = Arrays.copyOf(getNotasPorAmostra(i), getQtdeProvadores(i));

		arr = Arranjo.ordenar(arr);

		if ((arr.length % 2) == 0){
			idxInf = (int) Math.floor((arr.length / 2 + arr.length) / 2);
			q3 = arr[idxInf];
		}
		else{
			idxInf = arr.length / 2 + 1;
			q3 = arr[idxInf] + arr[idxInf + 1] / 2;
		}

		return q3;
	
	}

	public double getVariancia(int i) throws Exception{

		int qtdeProvadores = 9;
		double denominador = 0.0;
		double media = getMedia(i);

		double variancia = 0.0;

		for (int j=1; j <= qtdeProvadores; j++ ) {
			double diff = getNotaDouble(i, j) - media;
			denominador += Math.pow(diff, 2);
		}

		return denominador / getQtdeProvadores(i);

	}

	public double getDesvioPadrao(int i) throws Exception{

		return Math.sqrt(getVariancia(i));

	}

	/*
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	*/

	public int getQtdeProvadores(double m[][], int i){

		return m[i].length - 1;

	}

	public int getQtdeAmostras(double m[][]){

		return m.length;

	}

	public double getNota(double m[][], int i, int j){

		return m[i][j];

	}

	public double getNotaDouble(double m[][], int i, int j) {

		return getNota(m, i, j);

	}

	public double[] getNotasPorAmostra(double m[][], int i) {

		int qtdeProvadores = getQtdeProvadores(m, i);

		double amostraDoubleSemLabel[] = new double[qtdeProvadores];
		
			for (int j = 0; j < qtdeProvadores; j++)
				amostraDoubleSemLabel[j] = getNota(m, i, j + 1);
			

		return amostraDoubleSemLabel;

	}

	public int getQtdeTotalObs(double m[][]){

		int soma = 0;

		for (int i=0;i < m.length; i++)
			for (int j=1;j < m[0].length; j++)
				soma++;

		return soma;

	}

	public double getSomaTodasObs(double m[][]){

		double soma = 0;

		

		for (int i=0;i < m.length; i++)
			for (int j=1;j < m[i].length; j++)
				soma += m[i][j];
		
		

		return soma;

	}

	public double getSoma(double m[][], int i){

		double soma = 0;

		

		for (int j=1; j < m[i].length ; j++)
			soma += m[i][j];

		

		return soma;	

	}

	public double getMedia(double m[][], int i){

		return getSoma(m, i) / (double) getQtdeProvadores(m, i);
	
	}

}
