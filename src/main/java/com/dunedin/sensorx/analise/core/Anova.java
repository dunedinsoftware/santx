package com.dunedin.sensorx.analise.core;

public class Anova extends Amostras{

	public Anova(){
	}

	public double getCezao(){

		double soma = getSomaTodasObs();

		return  (double) soma * soma / getQtdeTotalObs();

	}

	

	public double getSomaQuadradoTodasObs(){

		double soma = 0;

		try {

		for (int i=0;i < amostras.length; i++)
			for (int j=1;j < amostras[i].length; j++)
				soma += Double.parseDouble(amostras[i][j]) * Double.parseDouble(amostras[i][j]);
		}
		catch(Exception e){


		}

		return soma;

	}



	public double getQuadradoSoman(int i){

		double soma = getSoma(i);

		return (soma * soma) / (double) getQtdeProvadores(i);

	}
	
	public double getSS(){

		return getSomaQuadradoTodasObs() - getCezao();

	}

	public double getGruposSS(){

		double soma = 0;

		for (int i=0;i < getQtdeAmostras(); i++)
			soma += getQuadradoSoman(i);

		return soma - getCezao();


	}

	public double getErroSS(){

		return getSS() - getGruposSS();

	}

	public double getGruposMS(){

		return (getGruposSS() / (getQtdeAmostras() - 1));

	}

	public double getErroMS(){

		return getErroSS() / (getQtdeTotalObs() - getQtdeAmostras());
	
	}

	public int getErroDF(){

		return getQtdeTotalObs() -  1 - (getQtdeAmostras() - 1);

	}

	public double getFCalculado(){

		return getGruposMS() / getErroMS();

	}

}
