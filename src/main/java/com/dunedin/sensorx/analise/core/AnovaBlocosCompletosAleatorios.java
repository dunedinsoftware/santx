package com.dunedin.sensorx.analise.core;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;
import java.util.Vector;
import java.nio.file.Paths;

public class AnovaBlocosCompletosAleatorios extends Amostras{

	double m3D[][][];

	public AnovaBlocosCompletosAleatorios(String[][] amostras){

		setAmostras(amostras);
		m3D = new double[getQtdeAmostras()][getQtdeProvadores(0)][2];
		criarBlocos();
	}

	public double getCezao(){

		double soma = getSomaTodasObs();

		return soma * soma / getQtdeTotalObs();

	}

	public double getSomaPorProvador(int j){

		double soma = 0;

		for (int i=0; i < m3D.length; i++){
		
			soma += m3D[i][j][0];

		}

		return soma;

	}

	public double getSomaPorAmostra(int i){

		double soma = 0;

		for (int j=0; j < m3D[i].length ; j++){
			soma += m3D[i][j][0];
		}

		return soma;	

	}

	public double getSomaQuadradoG(){

		double soma = 0;

		for (int i=0; i < m3D.length ; i++){
			soma += getSomaPorAmostra(i) * getSomaPorAmostra(i);
		}
		
		//System.out.printf("SomaQuadradoG = %.2f", soma);

		return soma;

	}

	public double getSomaQuadradoB(){

		double soma = 0;

		for (int j=0; j < m3D[0].length ; j++){
			soma += getSomaPorProvador(j) * getSomaPorProvador(j);
		}


		return soma;

	}

	public double getSomaQuadradoTodasObs(){

		double soma = 0;

		for (int i=0;i < m3D.length; i++){
			for (int j=0;j < m3D[i].length; j++)
				soma += m3D[i][j][0] * m3D[i][j][0];
		}
		
		return soma;

	}



	public double getQuadradoSoman(int i){

		double soma = getSomaPorAmostra(i);

		return (soma * soma) / (double) getQtdeProvadores(i);

	}
	
	public double getMediaPorAmostra(int i){

		return getSomaPorAmostra(i) / (double) getQtdeProvadores(i);
	
	}

	public double[][] ordenarBloco(double matriz[][]){

		int min = 0;
		double temp[];

		for (int i=0; i < matriz.length;i++){
			min = i;
			for (int j=i+1; j < matriz.length; j++)
				if (matriz[j][1] < matriz[min][1])
					min = j;

			temp = matriz[min];
			matriz[min] = matriz[i];
			matriz[i] = temp;	

		}
		return matriz;

	}


	public void randomizarBloco(int j){

		Random generator = new Random();
		double rank = 0;

		for (int i=0; i < m3D.length; i++){
			
		 	rank = (double) generator.nextInt(m3D.length) + 1;
		 	
		 	if (buscarRank(rank, j) == -1){
		 		m3D[i][j][1] = rank;
		 		//System.out.printf("%.2f/%.2f ", m3D[i][j][0], m3D[i][j][1]);
		 	}
		 	else
		 		i--;
		}
		//System.out.printf("\n");

	}

	
	public int buscarRank(double val, int j){

		int ret = -1;

		for (int i = 0; i < m3D.length; i++){
			
			if (m3D[i][j][1] == val){
				ret = i;
				break;	
			} 

		}
		
		return ret;

	}
	

	public void reordenar(){

		int np = getQtdeProvadores(0);
		int na = getQtdeAmostras();

		double matriz[][] = new double[na][2];
		double ord[][];

		for (int j=0; j < m3D[0].length; j++){

			for (int i=0; i < m3D.length; i++){

				matriz[i][0] = m3D[i][j][0];
				matriz[i][1] = m3D[i][j][1];

			}

			ord = ordenarBloco(matriz);

			for (int i=0; i < m3D.length; i++){

				m3D[i][j][0] = ord[i][0];
				m3D[i][j][1] = ord[i][1];
				//System.out.printf("%.2f/%.2f ", m3D[i][j][0], m3D[i][j][1]);
			}	
			//System.out.printf("\n");			
		}

	}

	public void criarBlocos(){

		int np = getQtdeProvadores(0);
		int na = getQtdeAmostras();

		m3D = new double[na][np][2];
		double arr[] = new double[na];
		double rnd[] = new double[na];
		double subtotal  = 0;
		double soma = 0;
		double temp = 0;

		try {

			for (int j=1; j <= np; j++){
				
				for (int i=0; i < na; i++){

					//System.out.printf("%d %d\n", i, j);

					m3D[i][j-1][0] = Double.parseDouble(getNota(i, j));
					//System.out.printf("%s ", amostras[i][j]);

				}
				//System.out.println();
			}

			for (int j=0; j < m3D[0].length; j++){

				randomizarBloco(j);
				//System.out.println();
				
			}

			//System.out.printf("\n\n");
			reordenar();

		}
		catch(Exception ex){
			ex.printStackTrace();
		}

	}

	public double getSSFator(){

		return getSomaQuadradoG() / (double) getQtdeProvadores(0) - getCezao();

	}

	public double getSSBloco(){

		return getSomaQuadradoB() / (double) getQtdeAmostras() - getCezao();

	}

	public double getSS(){

		return getSomaQuadradoTodasObs() - getCezao();

	}

	public double getErroSS(){

		
		return getSS() - getSSBloco() - getSSFator();

	}

	public double getFatorMS(){

		return getSSFator() / (double) (getQtdeAmostras() - 1);

	}

	public double getBlocoMS(){

		return getSSBloco() / (double) (getQtdeProvadores(0) - 1);

	}

	public double getErroMS(){

		return getErroSS() / (getQtdeTotalObs() - getQtdeAmostras());
	
	}

	public int getErroDF(){

		return getQtdeTotalObs() - (getQtdeAmostras() + getQtdeProvadores(0)) + 1;

	}

	public double getFFatorCalculado(){

		return getFatorMS() / getErroMS();

	}

	public double getFBlocoCalculado(){

		return getBlocoMS() / getErroMS();

	}

	/*
	public static void main (String args[]){

		AnovaBlocosAleatorios anova = new AnovaBlocosAleatorios();
		TabelaDistribuicao095f tabF = new TabelaDistribuicao095f();

		System.out.println("                              RESUMO DA ANOVA");
		System.out.println("ORIGEM DA VARIACAO         SS               DF                  MS                F");
		System.out.println("------------------------------------------------------------------");
		System.out.printf("TOTAL\t\t\t%.2f\t\t%d\n", anova.getSS(), (anova.getQtdeTotalObs() - 1));
		System.out.printf("   FATORES\t\t%.2f\t\t%d\t\t%.2f\t\t%.2f\n ", anova.getSSFator(), (anova.getQtdeAmostras() - 1), anova.getFatorMS(), anova.getFFatorCalculado());
		System.out.printf("  BLOCOS\t\t%.2f\t\t%d\t\t%.2f\t\t%.2f\n ", anova.getSSBloco(), (anova.getQtdeProvadores(0) - 1), anova.getBlocoMS(), anova.getFBlocoCalculado());		
		System.out.printf("  ERRO\t\t\t%.2f\t\t%d\t\t%.2f\n", anova.getErroSS(), anova.getErroDF(), anova.getErroMS());
		System.out.println("------------------------------------------------------------------");
		//System.out.println("F calculado = " + anova.getFCalculado());
		System.out.println("F tabela = " + tabF.getValorCritico(anova.getQtdeAmostras() - 1, anova.getErroDF()));

	}
	*/
}
