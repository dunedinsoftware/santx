package com.dunedin.sensorx.analise.core;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;
import java.util.Vector;
import java.nio.file.Paths;

public class AnovaTwoWay {

	private String amostrasCT[][] = null; //com tratamento
	private String amostrasST[][] = null; //sem tratamento

	public AnovaTwoWay(){
		
	}

	public void setAmostrasCT(String amostras[][]){
		this.amostrasCT = amostras;
	}

	public void setAmostrasST(String amostras[][]){
		this.amostrasST = amostras;
	}

	public int getQtdeProvadoresCT(int i){

		return amostrasCT[i].length - 1;

	}

	public int getQtdeAmostrasCT(){

		return amostrasCT.length;

	}

	public int getQtdeProvadoresST(int i){

		return amostrasST[i].length - 1;

	}

	public int getQtdeAmostrasST(){

		return amostrasST.length;

	}

	public int getQtdeTotalObs(){

		return getQtdeAmostrasCT() * getQtdeProvadoresCT(0) + getQtdeAmostrasST() * getQtdeProvadoresST(0);

	}

	public double getSomaCT(int i){

		double soma = 0;

		try {

		for (int j=1; j < amostrasCT[i].length ; j++)
			soma += Double.parseDouble(amostrasCT[i][j]);

		}
		catch(Exception ex){
		}

		return soma;	

	}

	public double getMediaCT(int i){

		return getSomaCT(i) / (double)(amostrasCT[i].length - 1);	

	}

	public double getMediaST(int i){

		return getSomaST(i) / (double)(amostrasST[i].length - 1);	

	}

	public double getSomaCTTodasAmostras(){

		double soma = 0;

		try {

		for (int i=0; i < amostrasCT.length ; i++)
			soma += getSomaCT(i);

		}
		catch(Exception ex){
		}

		return soma;	

	}

	public double getSomaST(int i){

		double soma = 0;

		try {

		for (int j=1; j < amostrasST[i].length ; j++)
			soma += Double.parseDouble(amostrasST[i][j]);

		}
		catch(Exception ex){
		}

		return soma;	

	}

	public double getSomaSTTodasAmostras(){

		double soma = 0;

		try {

		for (int i=0; i < amostrasST.length ; i++)
			soma += getSomaST(i);

		}
		catch(Exception ex){
		}

		return soma;	

	}

	public double getSomaCTSTPorAmostra(int i){

		return getSomaCT(i) + getSomaST(i);

	}

	public double getMediaCTSTPorAmostra(int i){

		return getMediaCT(i) + getMediaST(i);

	}

	public double getSomaCTSTTodasAmostras(){

		double soma = 0;
		for (int i=0; i < amostrasCT.length ; i++){
			soma += getSomaCTSTPorAmostra(i);
		}

		return soma;

	}

	public double getMediaCTSTTodasAmostras(){

		double soma = 0;
		for (int i=0; i < amostrasCT.length ; i++){
			soma += getMediaCTSTPorAmostra(i);
		}

		return soma;

	}

	public double getCezao(){

		double ret = getSomaCTSTTodasAmostras();

		return ret * ret / (double) getQtdeTotalObs();

	}

	public double getSomaQuadradoTodasObs(){

		double soma = 0;

		try {

			for (int i=0;i < amostrasCT.length; i++)
				for (int j=1;j < amostrasCT[i].length; j++)
					soma += Double.parseDouble(amostrasCT[i][j]) * Double.parseDouble(amostrasCT[i][j]);

			for (int i=0;i < amostrasST.length; i++)
				for (int j=1;j < amostrasST[i].length; j++)
					soma += Double.parseDouble(amostrasST[i][j]) * Double.parseDouble(amostrasST[i][j]);
		
		

		}
		catch(Exception e){


		}

		return soma;

	}

	public double getSS(){

		return getSomaQuadradoTodasObs() - getCezao();

	}


	public double getCelulas_SS(){

		double soma = 0;

		for (int i=0; i < amostrasCT.length; i++)
			soma += getSomaCT(i) * getSomaCT(i) + getSomaST(i) * getSomaST(i);

		return soma / getQtdeProvadoresCT(0) - getCezao();

	}

	public int getCelulas_DF(){

		return 2 * getQtdeAmostrasCT() - 1;

	}

	public double getErro_SS(){

		return getSS() - getCelulas_SS();

	}

	public int getErro_DF(){

		return 2 * getQtdeAmostrasCT() * (getQtdeProvadoresCT(0) - 1);

	}

	public double getFatorA_SS(){

		double ct = getSomaCTTodasAmostras();
		double st = getSomaSTTodasAmostras();

		return (ct * ct + st * st) / (getQtdeAmostrasCT() * getQtdeProvadoresCT(0)) - getCezao();

	}

	public int getFatorA_DF(){

		return 1;

	}

	public double getFatorB_SS(){

		double ctst = 0;
		double ret = 0;
		double soma = 0;

		for (int i=0; i < amostrasCT.length; i++){
			ctst = getSomaCTSTPorAmostra(i);
			soma += (ctst * ctst) ;
		}

		return (soma) / (getQtdeAmostrasCT() * getQtdeProvadoresCT(0)) - getCezao();

	}

	public int getFatorB_DF(){

		return getQtdeAmostrasCT() - 1;

	}

	public double getInteracaoFatoresAB_SS(){

		return getCelulas_SS() - getFatorA_SS() - getFatorB_SS();

	}

	public int getInteracaoFatoresAB_DF(){

		return getFatorA_DF() * getFatorB_DF();

	}

	public double getFatorA_MS(){

		return getFatorA_SS() / getFatorA_DF();

	}

	public double getFatorB_MS(){

		return getFatorB_SS() / getFatorB_DF();

	}

	public double getInteracaoFatoresAB_MS(){

		return getInteracaoFatoresAB_SS() / getInteracaoFatoresAB_DF();

	}

	public double getErro_MS(){

		return getErro_SS() / getErro_DF();

	}

	public double getFatorA_F(){
		return getFatorA_MS() / getErro_MS();
	}

	public double getFatorB_F(){
		return getFatorB_MS() / getErro_MS();
	}

	public double getInteracaoFatoresAB_F(){
		return getInteracaoFatoresAB_MS() / getErro_MS();
	}

}
