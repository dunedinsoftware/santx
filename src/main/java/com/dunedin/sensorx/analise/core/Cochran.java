package com.dunedin.sensorx.analise.core;

import java.util.Arrays;
import java.util.Vector;

public class Cochran extends Amostras{

	private String amostrasR[][] = null;

	public Cochran(String amostras[][]){

		setAmostras(amostras);
		amostrasR = new String[amostras.length][amostras[0].length];
		filtrar();

	}

	public void filtrar(){

		double soma = 0;
		Vector amostraEliminada = new Vector();

		try {

			for (int j=1;j < amostras[0].length; j++)
				for (int i=0;i < amostras.length; i++){
					if (amostras[i][j] != null){
						soma += Double.parseDouble(amostras[i][j]);
					}
					if (soma == amostras.length) amostraEliminada.add(j);
				}

			for (int i=0;i < amostras.length; i++){
				for (int j=0;j < amostras[0].length; j++){
					if ((amostras[i][j] != null) && (!amostraEliminada.contains(j))){
						amostrasR[i][j] = amostras[i][j];
						//System.out.println(amostras[i][j]);
					}
				}				
			}	
		}
		catch(Exception e){

		}

	}

	public int[] getArranjoB(){

		int soma = 0;int z = 0; int w = 0;
		Vector vec = new Vector();

		try {

			for (int j=1;j < amostrasR[0].length; j++){
				for (int i=0;i < amostrasR.length; i++){
					if (amostrasR[i][j] != null){
						soma += Integer.parseInt(amostrasR[i][j]);
					}
				}
				vec.add(soma);
				soma = 0;
			}

		}
		catch(Exception e){

		}

		int ret[] = new int[vec.size()];
		for (int i = 0; i < vec.size(); i++){
			ret[i] = (int) vec.get(i);
			//System.out.printf("...%d ", ret[i]);
		}

		return ret;

	}

	



	public int[] getArranjoG(){

		int soma = 0;
		Vector vec = new Vector();

		try {

		for (int i=0;i < amostrasR.length; i++){	
			for (int j=1;j < amostrasR[0].length; j++){	
				if (amostrasR[i][j] != null){
					soma += Integer.parseInt(amostrasR[i][j]);
				}
			}
			vec.add(soma);
			soma = 0;
		}	

		}
		catch(Exception e){

		}

		int ret[] = new int[vec.size()];
		for (int i = 0; i < vec.size(); i++){
			ret[i] = (int) vec.get(i);
			//System.out.println(">>>> " + ret[i]);
			//System.out.println(ret[i]);
		}

		return ret;

	}

	public int getSomaArranjo(int x[]){

		int soma = 0;

		for (int i=0; i < x.length ; i++){
			soma += x[i];
		}

		return soma;

	}

	public int getSomaQuadradoArranjo(int x[]){

		int soma = 0;

		for (int i=0; i < x.length ; i++){
			soma += x[i] * x[i];
		}

		return soma;

	}

	public int getA(){

		int tamreal = 0;

		for (int i=0; i < amostrasR.length ; i++){
			if (amostrasR[i] != null) tamreal++;
		}

		return tamreal;

	}

	public int getB(){

		int tamreal = 0;

		for (int i=1; i < amostrasR[0].length ; i++){
			if (amostrasR[0][i] != null) tamreal++;
		}

		return tamreal;

	}

	public double getQ(){

		double ret = 0;

		int a = getA();
		//int b = getB();
		int arrG[] = getArranjoG();
		double s2G = getSomaQuadradoArranjo(arrG);
		double sG  = getSomaArranjo(arrG);
		int arrB[] = getArranjoB();
		double s2B = getSomaQuadradoArranjo(arrB);
		double sB = getSomaArranjo(arrB);
		ret = (a - 1);
		ret = ret * (s2G - sG * sG / a);
		ret = ret / (sB - s2B / a);

		return ret;

	}

}
