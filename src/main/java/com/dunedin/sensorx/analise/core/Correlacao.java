package com.dunedin.sensorx.analise.core;

public class Correlacao extends Amostras{

	private int colX;
	private int colY;

	public Correlacao(int colX, int colY){
		this.colX = colX;
		this.colY = colY;
	}

	public double getSomaQuadradao(int i){

		double soma = 0;

		try {

		for (int j=1; j < amostras[colX].length ; j++)
			soma += Double.parseDouble(amostras[i][j]) * Double.parseDouble(amostras[i][j]);

		}
		catch(Exception ex){


		}

		return soma;

	}

	public double getSomaQuadradinho(int i){

		return getSomaQuadradao(i) - getSoma(i) * getSoma(i) / (double) getQtdeProvadores(i);

	}

	public double getSomaProdutoXY(){

		double soma = 0;

		try {

		for (int j=1; j < amostras[colX].length ; j++)
			soma += Double.parseDouble(amostras[colX][j]) * Double.parseDouble(amostras[colY][j]);

		}
		catch(Exception ex){


		}

		return soma;

	}

	public double getSomaProdutoxy(){

		return getSomaProdutoXY() - getSoma(colX) * getSoma(colY) / (double) getQtdeProvadores(colX);

	}

	public double getCoefCorrelacao(){

		return getSomaProdutoxy() / Math.sqrt(getSomaQuadradinho(colX) * getSomaQuadradinho(colY));

	}

	/************************************************************
	* F
	************************************************************/

	public double getFCalculado(){

		return (1 + Math.abs(getCoefCorrelacao())) / (1 - Math.abs(getCoefCorrelacao()));

	}



	
	

	

}