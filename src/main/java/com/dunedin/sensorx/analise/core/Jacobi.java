package com.dunedin.sensorx.analise.core;

import java.util.Vector;

public class Jacobi {

	private int j, iq, ip, i;
	private double tresh, theta, tau, t, sm, s, h, g, c;
	
	
	public Jacobi(double[][] a, int n, double[] d, double[][] v){

		Vector b = new Vector(n);
		Vector z = new Vector(n);

		for (ip = 0; ip <= n - 1; ip++){
			for (iq = 0;iq < n - 1;iq++) v[ip][iq] = 0.0;
			v[ip][ip] = 1.0;	
		}

		for (ip = 0; ip <= n - 1; ip++){
			d[ip] = a[ip][ip];
			b.add(ip, d[ip]); 
			z.add(ip, 0.0);
		}

		

		for (int i=0; i <= 50; i++){

			sm = 0.0;
			
			for (int ip = 0; ip <= n - 1; ip++){
				for (iq = ip + 1;iq <= n - 1;iq++) 
					sm += Math.abs(a[ip][iq]);
			}			

			if (sm == 0.0) return;

			if (i < 4){
				tresh = 0.2 * sm / (n * n);
			}
			else{
				tresh = 0.0;
			}

			for (int ip = 0; ip <= n - 1; ip++){
				for (int iq = ip + 1;iq <= n - 1; iq++) {
					g = 100.0 * Math.abs(a[ip][iq]);
					if ((i > 4) && ((double)Math.abs(d[ip] + g) == Math.abs(d[ip])) && ((double)(Math.abs(d[iq]) + g)) == (double) Math.abs(d[iq])){
						a[ip][iq] = 0.0;
					}
					else if (Math.abs(a[ip][iq]) > tresh){
						h = d[iq] - d[ip];
						if ((double)(Math.abs(h) + g) == (double) Math.abs(h)){
							t = (a[ip][iq])/h;

						}
						else {

							theta = .5 * h / (a[ip][iq]);
							t = 1.0 / (Math.abs(theta) + Math.sqrt(1.0 + theta * theta));
							if (theta < 0.0) t = -t;
						}

						c = 1.0 / Math.sqrt(1 + t*t);
						s = t * c;
						tau = s / (1.0 + c);
						h = t * a[ip][iq];

						double temp1 = (double)z.get(ip) - h;
						z.set(ip, temp1);

						double temp2 = (double)z.get(iq) + h;
						z.set(iq, temp2);

						d[ip] -= h;
						d[iq] += h;

						a[ip][iq] = 0.0;

						for (int j = 1;j <= ip - 1;j++){
							rotate(a, j, ip, j, iq);
						}

						for (int j = ip + 1;j <= iq - 1;j++){
							rotate(a, ip, j, j, iq);
						}

						for (int j = iq + 1;j <= n - 1;j++){
							rotate(a, ip, j, iq, j);
						}

						for (int j = 1;j <= n - 1;j++){
							rotate(v, j, ip, j, iq);
						}

						//System.out.println("rotacionando...");

						}

					}

				}

			}

			for (int ip = 0; ip < n; ip++){

				double temp5 = (double)b.get(ip) + (double) z.get(ip);
				b.set(ip, temp5);

				d[ip] = (double) b.get(ip);
				z.set(ip, 0.0);

			}	

		}

	private void rotate(double[][] a, int i, int j, int k, int l){

		double g = 0.0;
		double h = 0.0;

		g = a[i][j];
		h = a[k][l];
		a[i][j] = g - s * (h + g * tau);
		a[k][l] = h + s * (g - h * tau);

	}
	
	

}