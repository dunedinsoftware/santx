/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dunedin.sensorx.analise.core;

/**
 *
 * @author dragon
 */
import java.io.IOException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;
import java.util.ArrayList;
import java.util.List;
import java.util.Arrays;
import java.util.Comparator;
import java.util.stream.Collectors;
import java.util.Timer;
import java.util.TimerTask;

public class Kmeansplusplus {
    
    private static final int QTD_CENTROS = 3; 

    Stream<String> lines;
    Ponto minMean = null;
    Ponto[] vertices = null;
    Ponto[] cMeans = null;
    List<Ponto> listaV = new ArrayList<Ponto>();

    public Kmeansplusplus(String[][] matriz){

        cMeans = new Ponto[QTD_CENTROS];

        try {

            OutputStream out2 = new FileOutputStream("output.txt");
            PrintStream out = new PrintStream(out2);

            /*
            lines = Files.lines(Paths.get("input.txt"));
            String[][] matriz = lines.map(line -> line.split(";")).toArray(String[][]::new);
            */
            
            /* Pula os rotulos */
            for (int i=1; i < matriz[0].length; i++){
                //System.out.println(matriz[0][i] + "");
                //System.out.println(matriz[1][i] + "");
                double x = Double.parseDouble(matriz[0][i]);
                double y = Double.parseDouble(matriz[1][i]);

                listaV.add(new Ponto(x, y));
            }

            /*---------------------------------------------------------
            Qdo puder usar RANDOMISE para aleatorizar o primeiro centro
            ----------------------------------------------------------*/
            cMeans[0] = listaV.get(listaV.size() / 2);
            cMeans[0].setClusterId(0.0);

            /****************************************************
            * K-Means++
            ****************************************************/
            int i = 0;
            while (i < cMeans.length && cMeans[i] != null){

                Ponto nextCentro = null;
                double maxSqrDist = 0.0;
                double temp = 0.0;

                for (Ponto vertice : listaV){

                    if (!cMeans[i].equals(vertice)){

                        temp = Math.pow(calcDist(vertice, cMeans[i]), 2);
                        //System.out.printf("%.2f\n", temp);
                        if (temp > maxSqrDist){
                            maxSqrDist = temp;
                            nextCentro = vertice; 
                        }

                    }

                }

                if (i + 1 < cMeans.length){
                    //if (!isCadastrado(cMeans, nextCentro)){
                        cMeans[i + 1] = nextCentro;
                        cMeans[i + 1].setClusterId(i + 1);
                    //}
                }

                //System.out.printf("%.2f, %.2f, %.2f\n", cMeans[i].getX(), cMeans[i].getY(), cMeans[i].getClusterId());
                i++;
            } 
            /*
            for (int j=0; j < cMeans.length && cMeans[j] != null; j++){
                System.out.printf("Centro: ");
                System.out.println(cMeans[j]);
            }
            */

            /****************************************************
            * Algoritmo de Lloyd
            *****************************************************/
            vertices = listaV.toArray(new Ponto[listaV.size()]);

            boolean alterou = true; int cont = 0;
            while(alterou && cont++ <= QTD_CENTROS){
                alterou = atribuir2Cluster(vertices, cMeans);
                atualizarK();
            }
            /*
            System.out.println();
            for (Ponto p : getMedidas()){
                System.out.println(p);
            }
            */

        } 
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<Ponto> getMedidas(){
        Arrays.sort(vertices, Comparator.comparingDouble(o -> o.getX()));
        return new ArrayList<Ponto>(Arrays.asList(vertices));
    }

    public Ponto[] getMedidasSemCentros(){
        Arrays.sort(vertices, Comparator.comparingDouble(o -> o.getClusterId()));
        return vertices;
    }

    public double[][] getArrayMedidas(){
        Arrays.sort(vertices, Comparator.comparingDouble(o -> o.getX()));
        double[][] v = new double[2][vertices.length];
        for (int i=0; i < vertices.length; i++){
            v[0][i] = vertices[i].getX();
            v[1][i] = vertices[i].getY();
        }
        return v;
    }

    public Ponto[] getArrayKmedias(){
        return cMeans;
    }

    public List<Ponto> getKmedias(){
        return new ArrayList<Ponto>(Arrays.asList(cMeans));
    }
    
    /*
    public static void main(String args[]) {

        try {
            Kmeansplusplus kmeans = new Kmeansplusplus();
            
            Scatterplot grafico = new Scatterplot(kmeans.getMedidasSemCentros(), kmeans.getArrayKmedias());
        }
        catch(Exception e){
            e.printStackTrace();
        }
        
    }
    */
    
    private double calcDist(Ponto a, Ponto b){
        return Math.sqrt( Math.pow((a.getX() - b.getX()), 2) + Math.pow((a.getY() - b.getY()), 2));
    }

    private boolean atribuir2Cluster(Ponto[] vertices, Ponto[] cMeans){

        boolean houveAlteracaoCluster = false;

        for (int i=0; i < vertices.length; i++){

            double[][] aTemp = new double[cMeans.length][2]; 

            for (int j=0; j < cMeans.length && cMeans[j] != null; j++){
        
                aTemp[j][0] = calcDist(vertices[i], cMeans[j]);
                aTemp[j][1] = cMeans[j].getClusterId();

            }

            Arrays.sort(aTemp, Comparator.comparingDouble(o -> o[0]));

            double clusterIdOld = vertices[i].getClusterId();
            double clusterId    = aTemp[0][1];

            vertices[i].setClusterId(clusterId);

            houveAlteracaoCluster = Double.compare(clusterId, clusterIdOld) == 0; 

        }       

        return houveAlteracaoCluster; 

    }

    private void atualizarK(){

        for (int i=0; i < cMeans.length && cMeans[i] != null; i++){

            double somaX = 0;
            double somaY = 0;
            int      qtd = 0;

            for (int j=0; j < vertices.length; j++){
        
                if (Double.compare(vertices[j].getClusterId(), cMeans[i].getClusterId()) == 0){

                    somaX += vertices[j].getX();
                    somaY += vertices[j].getY();
                    qtd++;  

                }

            }

            cMeans[i].setX(somaX / qtd);
            cMeans[i].setY(somaY / qtd);

            //System.out.println("Atualizando média do cluster...");
            //System.out.println(cMeans[i]);
            
        }    

    }

    public boolean isCadastrado(Ponto[] a, Ponto p){

        boolean ret = false;
        for (int i=0; i < a.length; i++){
            if (a[i] != null){
                if (a[i].equals(p)){
                    ret = true; 
                    break;
                }
            }
        }
        return ret;

    }

}
