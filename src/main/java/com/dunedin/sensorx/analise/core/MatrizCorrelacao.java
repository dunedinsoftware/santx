package com.dunedin.sensorx.analise.core;

public class MatrizCorrelacao extends Amostras{

	public MatrizCorrelacao(){
	
	}

	public MatrizCorrelacao(double[][] x){

	}

	public double getSomaQuadradao(int i) throws Exception{

		double soma = 0;
		double arrNotas[] = getNotasPorAmostra(i);

		for (int j=1; j < arrNotas.length; j++)
			soma += getNotaDouble(i, j) * getNotaDouble(i, j);

		return soma;

	}

	public double getSomaQuadradinho(int i) throws Exception{

		return getSomaQuadradao(i) - getSoma(i) * getSoma(i) / (double) getQtdeProvadores(i);
		
	}

	public double getSomaProdutao(int i1, int i2) throws Exception{

		double soma = 0;
		double arrNotas1[] = getNotasPorAmostra(i1);
		double arrNotas2[] = getNotasPorAmostra(i2);

		for (int j=1; j < arrNotas1.length; j++)
			soma += getNotaDouble(i1, j) * getNotaDouble(i2, j);

		return soma;

	}

	public double getSomaProdutinho(int i1, int i2) throws Exception{

		return getSomaProdutao(i1, i2) - getSoma(i2) * getSoma(i1) / (double) getQtdeProvadores(i1);
		
	}

	public double getCoefCorrelacao(int i1, int i2) throws Exception{

		//return getSomaProdutinho(i1, i2) / Math.sqrt(getSomaQuadradinho(i1) * getSomaQuadradinho(i2));
		return getSomaProdutinho(i1, i2) / getSomaQuadradinho(i1);
	}
	
	/*
	public double getFCalculado(){

		return (1 + Math.abs(getCoefCorrelacao())) / (1 - Math.abs(getCoefCorrelacao()));

	}
	*/

	public double[][] getMatrizCorrelacao() throws Exception{

		int qtdeAmostras = getQtdeAmostras();
		//int qtdeProvadores = getQtdeProvadores();

		double matriz[][] = new double[qtdeAmostras][qtdeAmostras]; //Matriz deve ser quadrada

		for (int i=0; i < qtdeAmostras; i++)
			for (int j=0; j < qtdeAmostras; j++)
				matriz[i][j] = getCoefCorrelacao(i, j);

		return matriz;	

	}

	/*
	/////////////////////////////////////////////////////////////////////////////////////////////////

	*/

	public double[][] getMatrizCorrelacao(double[][] m)  throws Exception{

		int qtdeAmostras = getQtdeAmostras(m);
		//int qtdeProvadores = getQtdeProvadores();

		double matriz[][] = new double[qtdeAmostras][qtdeAmostras]; //Matriz deve ser quadrada

		for (int i=0; i < qtdeAmostras; i++)
			for (int j=0; j < qtdeAmostras; j++)
				matriz[i][j] = getCoefCorrelacao(m, i, j);

		return matriz;	

	}

	public double getSomaQuadradao(double m[][], int i) throws Exception{

		double soma = 0;
		double arrNotas[] = getNotasPorAmostra(m, i);

		for (int j=1; j < arrNotas.length; j++)
			soma += getNotaDouble(m, i, j) * getNotaDouble(m, i, j);

		return soma;

	}

	public double getSomaQuadradinho(double m[][], int i) throws Exception{

		return getSomaQuadradao(m, i) - getSoma(m, i) * getSoma(m, i) / (double) getQtdeProvadores(m, i);
		
	}

	public double getSomaProdutao(double m[][], int i1, int i2) throws Exception{

		double soma = 0;
		double arrNotas1[] = getNotasPorAmostra(m, i1);
		double arrNotas2[] = getNotasPorAmostra(m, i2);

		for (int j=1; j < arrNotas1.length; j++)
			soma += getNotaDouble(m, i1, j) * getNotaDouble(m, i2, j);

		return soma;

	}

	public double getSomaProdutinho(double m[][], int i1, int i2) throws Exception{

		return getSomaProdutao(m, i1, i2) - getSoma(m, i2) * getSoma(m, i1) / (double) getQtdeProvadores(m, i1);
		
	}

	public double getCoefCorrelacao(double m[][], int i1, int i2) throws Exception{

		return getSomaProdutinho(m, i1, i2) / Math.sqrt(getSomaQuadradinho(m, i1) * getSomaQuadradinho(m, i2));

	}

}