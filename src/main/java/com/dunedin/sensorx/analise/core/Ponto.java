/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dunedin.sensorx.analise.core;

/**
 *
 * @author dragon
 */
public class Ponto {

    double x = 0.0;
    double y = 0.0;
    double clusterId = 0;

    public Ponto(double x, double y){
        this.x = x;
        this.y = y;
        this.clusterId = 0;
    }

    public Ponto(double x, double y, int clusterId){
        this.x = x;
        this.y = y;
        this.clusterId = clusterId;
    }

    public double getX(){return this.x;}
    public void setX(double x){this.x = x;}
    public double getY(){return this.y;}
    public void setY(double y){this.y = y;}
    public double getClusterId(){return this.clusterId;}
    public void setClusterId(double clusterId){this.clusterId = clusterId;}

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Ponto that = (Ponto) o;
        return this.x == that.x && this.y == that.y && this.clusterId == that.clusterId;
    }

    @Override
    public String toString(){
        return ("x = " + this.x + ", y = " + this.y + ", clusterId = " + this.clusterId);
    }

}
