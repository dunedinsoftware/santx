package com.dunedin.sensorx.analise.core;

public class Procrustes extends MatrizCorrelacao{

	public Procrustes(){

	}

	public double[][] getMatrizResiduos() throws Exception{

		int qtdeAmostras = getQtdeAmostras();
		int qtdeProvadores = getQtdeProvadores(0);

		double mresiduos[][] = new double[qtdeAmostras][qtdeProvadores];

		for (int i=0; i<qtdeAmostras;i++)
			for (int j=0; j<qtdeProvadores;j++)
				mresiduos[i][j] = Double.parseDouble(amostras[i][j + 1]) - getMedia(i);

		return mresiduos;

	}


	public double[] getEigenValores() throws Exception{

		double mres[][] = getMatrizResiduos();

		double mcorr[][] = getMatrizCorrelacao(mres);		

		double eigenValores[] = new double[mcorr.length]; 
		double eigenVetores[][] = new double[mcorr.length][mcorr.length];

		Jacobi jacobi = new Jacobi(mcorr, mcorr.length, eigenValores, eigenVetores);

		return eigenValores;

	}

	public double[] ordenarArr(double arr[] ){

		int min = 0;
		double temp = 0;

		for (int i=0; i < arr.length;i++){
			min = i;
			for (int j=i+1; j < arr.length; j++)
				if (arr[j] < arr[min])
					min = j;

			temp = arr[min];
			arr[min] = arr[i];
			arr[i] = temp;	

		}
		return arr;

	}

	public double getPesoCP(double arr[], int k) throws Exception {

		arr = ordenarArr(arr);

		double total = 0.0;
		for (int i=0; i < arr.length; i++) total += arr[i];
		return Math.round(100 * arr[k] / total);	

	}

	public double[][] getEigenVetores() throws Exception{

		double mres[][] = getMatrizResiduos();

		double mcorr[][] = getMatrizCorrelacao(mres);

		double eigenValores[] = new double[mcorr.length]; 
		double eigenVetores[][] = new double[mcorr.length][mcorr.length];

		Jacobi jacobi = new Jacobi(mcorr, mcorr.length, eigenValores, eigenVetores);

		return eigenVetores; 

	}

	public double[][] getMatrizInversa(double m[][]){

		double mInv[][] = new double[m[0].length][m.length];

		for (int i=0; i<m.length;i++)
			for (int j=0; j<m[0].length;j++)
				mInv[j][i] = m[i][j];

		return mInv;

	}



	public double[][] getScores() throws Exception {

		double mres[][] = getMatrizInversa(getMatrizResiduos());
		double mev[][]  = getEigenVetores();
		double msc[][]  = new double[mres.length][mev.length];
		double temp 	= 0.0;

		for (int i = 0; i < mres.length; i++){
			for (int j = 0; j < mev.length; j++){
				for (int k = 0; k < mev.length; k++){
					temp += mres[i][k] * mev[k][j]; 					
				}
				//System.out.println(i + " " + j + " = " + temp);
				msc[i][j] = temp;
				temp = 0.0;
			}
		}		

		return getMatrizInversa(msc);		

	}

}