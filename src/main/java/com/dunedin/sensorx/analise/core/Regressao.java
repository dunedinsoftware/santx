package com.dunedin.sensorx.analise.core;

public class Regressao extends Amostras{

	private int colX;
	private int colY;

	public Regressao(int colX, int colY){
		this.colX = colX;
		this.colY = colY;
	}

	public double getSomaQuadradao(int i){

		double soma = 0;

		try {

		for (int j=1; j < amostras[colX].length ; j++)
			soma += Double.parseDouble(amostras[i][j]) * Double.parseDouble(amostras[i][j]);

		}
		catch(Exception ex){


		}

		return soma;

	}

	public double getSomaQuadradinho(int i){

		return getSomaQuadradao(i) - getSoma(i) * getSoma(i) / (double) getQtdeProvadores(i);

	}

	public double getSomaProdutoXY(){

		double soma = 0;

		try {

		for (int j=1; j < amostras[colX].length ; j++)
			soma += Double.parseDouble(amostras[colX][j]) * Double.parseDouble(amostras[colY][j]);

		}
		catch(Exception ex){


		}

		return soma;

	}

	public double getSomaProdutoxy(){

		return getSomaProdutoXY() - getSoma(colX) * getSoma(colY) / (double) getQtdeProvadores(colX);

	}

	public double getBeta(){

		return getSomaProdutoxy() / getSomaQuadradinho(colX);

	}

	public double getAlfa(){

		return getMedia(colY) - getBeta() * getMedia(colX);
	
	}

	/************************************************************
	* ANOVA
	************************************************************/

	public double getTotalSS(){

		return getSomaQuadradinho(colY);

	}

	public double getRegressaoSS(){

		return getSomaProdutoxy() * getSomaProdutoxy() / getSomaQuadradinho(colX);

	}

	public double getCoefDeterminacao(){

		return getRegressaoSS() / getTotalSS();

	}

	public double[] ordenarArr(double arr[] ){

		int min = 0;
		double temp = 0;

		for (int i=0; i < arr.length;i++){
			min = i;
			for (int j=i+1; j < arr.length; j++)
				if (arr[j] < arr[min])
					min = j;

			temp = arr[min];
			arr[min] = arr[i];
			arr[i] = temp;	

		}
		return arr;

	}

	public double getMaximo(String data[][], int col){

		double arr[] = new double[data[col].length - 1];

		for (int j=1; j < data[col].length; j++){
			arr[j-1] = Double.parseDouble(data[col][j]);	
		}

		arr = ordenarArr(arr);
		
		return arr[arr.length - 1];

	}

	public double getMinimo(String data[][], int col){

		double arr[] = new double[data[col].length - 1];

		for (int j=1; j < data[col].length; j++){
			arr[j-1] = Double.parseDouble(data[col][j]);	
		}

		arr = ordenarArr(arr);
		
		return arr[0];

	}


}