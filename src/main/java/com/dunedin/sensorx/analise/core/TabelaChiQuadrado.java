package com.dunedin.sensorx.analise.core;

import java.util.HashMap;
import java.util.Map;

public class TabelaChiQuadrado {

	Map<Integer, Map<Double, Double>> map = new HashMap<>();

	Map<Double, Double> map1 = new HashMap<>();
	Map<Double, Double> map2 = new HashMap<>();
	Map<Double, Double> map3 = new HashMap<>();
	Map<Double, Double> map4 = new HashMap<>();
	Map<Double, Double> map5 = new HashMap<>();
	Map<Double, Double> map6 = new HashMap<>();
	Map<Double, Double> map7 = new HashMap<>();
	Map<Double, Double> map8 = new HashMap<>();
	Map<Double, Double> map9 = new HashMap<>();
	Map<Double, Double> map10 = new HashMap<>();
	Map<Double, Double> map11 = new HashMap<>();
	Map<Double, Double> map12 = new HashMap<>();
	Map<Double, Double> map13 = new HashMap<>();
	Map<Double, Double> map14 = new HashMap<>();
	Map<Double, Double> map15 = new HashMap<>();
	Map<Double, Double> map16 = new HashMap<>();
	Map<Double, Double> map17 = new HashMap<>();
	Map<Double, Double> map18 = new HashMap<>();
	Map<Double, Double> map19 = new HashMap<>();
	Map<Double, Double> map20 = new HashMap<>();
	Map<Double, Double> map21 = new HashMap<>();
	Map<Double, Double> map22 = new HashMap<>();
	Map<Double, Double> map23 = new HashMap<>();
	Map<Double, Double> map24 = new HashMap<>();
	Map<Double, Double> map25 = new HashMap<>();
	Map<Double, Double> map30 = new HashMap<>();
	Map<Double, Double> map40 = new HashMap<>();
	Map<Double, Double> map50 = new HashMap<>();
	Map<Double, Double> map60 = new HashMap<>();
	Map<Double, Double> map70 = new HashMap<>();
	Map<Double, Double> map80 = new HashMap<>();
	Map<Double, Double> map90 = new HashMap<>();
	Map<Double, Double> map100 = new HashMap<>();


	public TabelaChiQuadrado(){

		//m = 1
		map1.put(.950, 3.841 );
		map1.put(.05, .0039 );
		
		map.put(1, map1);

		//m = 2
		map2.put(.950, 5.991 );
		map2.put(.05, .1026);
		map.put(2 , map2);

		//m = 3
		map3.put(.950, 7.815 );
		map3.put(.05, .3518);
		map.put(3 , map3);

		//m = 4
		map4.put(.950, 9.488 );
		map4.put(.05, .7107);
		map.put(4 , map4);

		//m = 5
		map5.put(.950, 11.07 );
		map5.put(.05, 1.145);
		map.put(5 , map5);

		//m = 6
		map6.put(.950, 12.59 );
		map6.put(.05, 1.635);
		map.put(6, map6);

		//m = 7
		map7.put(.950, 14.07 );
		map7.put(.05, 2.167);
		map.put(7 , map7);

		//m = 8
		map8.put(.950, 15.51 );
		map8.put(.05, 2.732);
		map.put(8 , map8);
		
		//m = 9
		map9.put(.950, 16.92  );
		map9.put(.05, 3.325);
		map.put(9, map9);

		//m = 10
		map10.put(.950, 18.31 );
		map10.put(.05, 3.94);
		map.put(10 , map10);


		//m = 11
		map11.put(.950, 19.68 );
		map11.put(.05, 4.575);
		map.put(11 , map11);




		//m = 12
		map12.put(.950,  21.03);
		map12.put(.05, 5.226);
		map.put(12 , map12);




		//m = 13
		map13.put(.950, 22.36 );
		map13.put(.05, 5.892);
		map.put(13 , map13);



		//m = 14
		map14.put(.950, 23.68 );
		map14.put(.05, 6.571);
		map.put(14 , map14);



		//m = 15
		map15.put(.950, 25.0 );
		map15.put(.05, 7.261);
		map.put(15 , map15);




		//m = 16
		map16.put(.950, 26.3 );
		map16.put(.05, 7.962);
		map.put(16 , map16);



		//m = 17
		map17.put(.950, 27.59 );
		map17.put(.05, 8.672);
		map.put(17 , map17);




		//m = 18
		map18.put(.950, 28.87 );
		map18.put(.05, 9.39);
		map.put(18 , map18);




		//m = 19
		map19.put(.950, 30.14 );
		map19.put(.05, 10.12);
		map.put(19 , map19);




		//m = 20
		map20.put(.950, 31.41 );
		map20.put(.05, 10.85);
		map.put(20 , map20);




		//m = 21
		map21.put(.950, 32.67 );
		map21.put(.05, 11.59);
		map.put(21 , map21);




		//m = 22
		map22.put(.950, 33.92);
		map22.put(.05, 12.34);
		map.put(22 , map22);




		//m = 23
		map23.put(.950, 35.17 );
		map23.put(.05, 13.09);
		map.put(23 , map23);




		//m = 24
		map24.put(.950, 36.42);
		map24.put(.05, 13.85);
		map.put(24 , map24);




		//m = 25
		map25.put(.950, 37.65 );
		map25.put(.05, 14.61 );
		map.put(25 , map25);




	



		//m = 30
		map30.put(.950, 43.77 );
		map30.put(.05, 18.49);
		map.put(30 , map30);




		//m = 40
		map40.put(.950, 55.76 );
		map40.put(.05, 26.51);
		map.put(40 , map40);

		//m = 50
		map50.put(.950, 67.51 );
		map50.put(.05, 34.76);
		map.put(50 , map50);


		//m = 60
		map60.put(.950, 79.08 );
		map60.put(.05, 43.19);
		map.put(60 , map60);

		//m = 70
		map70.put(.950, 90.53 );
		map70.put(.05, 51.74);
		map.put(70 , map70);

		//m = 80
		map80.put(.950, 101.9 );
		map80.put(.05, 60.39);
		map.put(80 , map80);

		//m = 90
		map90.put(.950, 113.1 );
		map90.put(.05, 69.13);
		map.put(90 , map90);

		//m = 100
		map100.put(.950, 124.3 );
		map100.put(.05, 77.93);
		map.put(100 , map100);




		



	}

	public double getValorCritico(double icDecimal, int grausLiberdadeErro){

		double vc = 0;

		//Map<Integer, Map<Double, Double>> map = new HashMap<>();
		Map<Double, Double> x = new HashMap<Double, Double>();

		x = map.get(grausLiberdadeErro);

		double chave = 0;
		double prevIc = 0;
		double proxIc = 0;
		for (Map.Entry<Double, Double> el : x.entrySet()){
			chave = el.getKey();
			if (chave > icDecimal){
				proxIc = chave;
				break;
			}
		}
		double chaveAnterior = .005;
		for (Map.Entry<Double, Double> el : x.entrySet()){
			chave = el.getKey();
			if (chave > icDecimal){
				prevIc = chaveAnterior;
				break;
			}
			chaveAnterior = chave;
		}

		if (prevIc == 0) prevIc = .005;
		if (proxIc == 0) proxIc = .995;
		
		try {
			vc = x.get(icDecimal);
		}
		catch(NullPointerException npe){
			vc = (x.get(prevIc) + x.get(proxIc)) / (double) 2;
		}

		return vc;
	}

	/*
	public static void main (String args[]){

		TabelatStudent t = new TabelatStudent();

		double valorCritico = 0;

		try {

			valorCritico = t.getValorCritico(Double.parseDouble(args[0]), Integer.parseInt(args[1]));

		}
		catch(Exception ex){
			System.out.println("Valor nao encontrado");
		}

		System.out.printf("%.3f\n", valorCritico);

	}
	*/


}

