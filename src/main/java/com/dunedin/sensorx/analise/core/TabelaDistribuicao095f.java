package com.dunedin.sensorx.analise.core;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;


public class TabelaDistribuicao095f {

	//Map<Double, Map<Integer, Double>> map = new HashMap<Double, Map<Integer, Double>>();
	Map<Integer, Map<Integer, Double>> map = new HashMap<>();

		// p = .55
	//Map<Integer, Double> map55 = new HashMap<Integer, Double>();
	Map<Integer, Double> map1 = new HashMap<>();
	Map<Integer, Double> map2 = new HashMap<>();
	Map<Integer, Double> map3 = new HashMap<>();
	Map<Integer, Double> map4 = new HashMap<>();
	Map<Integer, Double> map5 = new HashMap<>();
	Map<Integer, Double> map6 = new HashMap<>();
	Map<Integer, Double> map7 = new HashMap<>();
	Map<Integer, Double> map8 = new HashMap<>();
	Map<Integer, Double> map9 = new HashMap<>();
	Map<Integer, Double> map10 = new HashMap<>();
	Map<Integer, Double> map15 = new HashMap<>();
	Map<Integer, Double> map20 = new HashMap<>();
	Map<Integer, Double> map25 = new HashMap<>();
	Map<Integer, Double> map30 = new HashMap<>();
	Map<Integer, Double> map40 = new HashMap<>();
	Map<Integer, Double> map60 = new HashMap<>();
	Map<Integer, Double> map120 = new HashMap<>();


	public TabelaDistribuicao095f(){

		//m = 1
		
		map1.put(1,  161.4 );
		map1.put(2 , 18.51 ) ;
		map1.put(3 , 10.13 );
		map1.put(4 , 7.71 );
		map1.put(5 , 6.61 );
		map1.put(6 , 5.99 );
		map1.put(7 , 5.59 );
		map1.put(8 , 5.32 );
		map1.put(9,  5.12 );
		map1.put(10 ,4.96 );
		map1.put(15 , 4.54);
		map1.put(20 , 4.35);
		map1.put(30 , 4.17);
		map1.put(40 , 4.08 );
		map1.put(60 , 4.00 );
		map1.put(120 , 3.92);

		map.put(1, map1);

		//m = 2
	map2.put(1,  199.5 );
		map2.put(2 ,  19.0) ;
		map2.put(3 ,  9.55);
		map2.put(4 ,  6.94);
		map2.put(5 ,  5.79);
		map2.put(6 ,  5.14);
		map2.put(7 ,  4.74);
		map2.put(8 ,  4.46);
		map2.put(9,   4.26);
		map2.put(10 , 4.1);
		map2.put(15 , 3.68);
		map2.put(18,  3.58);
		map2.put(20 , 3.49);
		map2.put(30 , 3.32);
		map2.put(40 , 3.23 );
		map2.put(60 , 3.15 );
		map2.put(120 , 3.07);

		map.put(2, map2);

		//m = 3
	map3.put(1,   215.7);
		map3.put(2 , 19.16 ) ;
		map3.put(3 , 9.28 );
		map3.put(4 , 6.59 );
		map3.put(5 , 5.41 );
		map3.put(6 , 4.76 );
		map3.put(7 , 4.35 );
		map3.put(8 , 4.07 );
		map3.put(9,  3.86 );
		map3.put(10 , 3.71);
		map3.put(12, 3.52);
		map3.put(15 , 3.29);
		map3.put(20 , 3.10);
		map3.put(30 , 2.92);
		map3.put(40 , 2.84 );
		map3.put(60 , 2.76 );
		map3.put(116, 2.68);
		map3.put(120 , 2.68);

		map.put(3, map3);

		//m = 4
	map4.put(1,  224.6 );
		map4.put(2 ,  19.25) ;
		map4.put(3 ,  9.12);
		map4.put(4 ,  6.39);
		map4.put(5 ,  5.19);
		map4.put(6 ,  4.53);
		map4.put(7 ,  4.12);
		map4.put(8 ,  3.84);
		map4.put(9,   3.63);
		map4.put(10 , 3.48);
		map4.put(15 , 3.06);
		map4.put(20 , 2.87);
		map4.put(25, 2.78); //<<<<
		map4.put(30 , 2.69);
		map4.put(40 , 2.61 );
		map4.put(60 , 2.53 );
		map4.put(120 , 2.45 );

		map.put(4, map4);

		//m = 5
	map5.put(1, 230.2  );
		map5.put(2 , 19.3 ) ;
		map5.put(3 , 9.01 );
		map5.put(4 , 6.26 );
		map5.put(5 , 5.05 );
		map5.put(6 , 4.39 );
		map5.put(7 , 3.97 );
		map5.put(8 , 3.69 );
		map5.put(9,  3.48 );
		map5.put(10 , 3.33);
		map5.put(15 , 2.90);
		map5.put(20 , 2.71);
		map5.put(30 , 2.53);
		map5.put(40 , 2.45 );
		map5.put(60 , 2.37 );
		map5.put(120 , 2.29 );

		map.put(5, map5);

		//m = 6
	map6.put(1, 234.0  );
		map6.put(2 , 19.33 ) ;
		map6.put(3 , 8.94 );
		map6.put(4 , 6.16 );
		map6.put(5 , 4.95 );
		map6.put(6 , 4.28 );
		map6.put(7 , 3.87 );
		map6.put(8 , 3.58 );
		map6.put(9,  3.37 );
		map6.put(10 , 3.22);
		map6.put(15 , 2.79);
		map6.put(20 , 2.60);
		map6.put(30 , 2.42);
		map6.put(40 , 2.34 );
		map6.put(60 , 2.25 );
		map6.put(120 , 2.17);

		map.put(6, map6);

		//m = 7 
	map7.put(1, 236.8  );
		map7.put(2 , 19.35 ) ;
		map7.put(3 , 8.89 );
		map7.put(4 , 6.09 );
		map7.put(5 , 4.88 );
		map7.put(6 , 4.21 );
		map7.put(7 , 3.79 );
		map7.put(8 , 3.5 );
		map7.put(9,  3.29 );
		map7.put(10 ,3.14 );
		map7.put(15 ,2.71 );
		map7.put(20 ,2.51 );
		map7.put(30 ,2.33 );
		map7.put(40 ,2.25  );
		map7.put(60 ,2.17  );
		map7.put(120,2.09 );

		map.put(7, map7);

		//m = 8
	map8.put(1, 238.9  );
		map8.put(2 , 19.37 ) ;
		map8.put(3 , 8.85 );
		map8.put(4 , 6.04 );
		map8.put(5 , 4.82 );
		map8.put(6 , 4.15 );
		map8.put(7 , 3.73 );
		map8.put(8 , 3.44 );
		map8.put(9,  3.23 );
		map8.put(10 , 3.07);
		map8.put(15 , 2.64);
		map8.put(20 , 2.45);
		map8.put(30 , 2.27);
		map8.put(40 , 2.18 );
		map8.put(60 , 2.10 );
		map8.put(120 , 2.02);

		map.put(8, map8);

		//m = 9
		map9.put(1, 240.5  );
		map9.put(2 , 19.38 ) ;
		map9.put(3 , 8.81 );
		map9.put(4 , 6.0 );
		map9.put(5 , 4.77 );
		map9.put(6 , 4.1 );
		map9.put(7 , 3.68 );
		map9.put(8 , 3.39 );
		map9.put(9,  3.18 );
		map9.put(10 , 3.02);
		map9.put(15 , 2.59);
		map9.put(20 , 2.39);
		map9.put(30 , 2.21);
		map9.put(40 , 2.12 );
		map9.put(60 , 2.04 );
		map9.put(120 , 1.96);

		map.put(9, map9);

		//m = 10
		map10.put(1, 241.9  );
		map10.put(2 , 19.4 ) ;
		map10.put(3 , 8.79 );
		map10.put(4 , 5.96 );
		map10.put(5 , 4.74 );
		map10.put(6 , 4.06 );
		map10.put(7 , 3.64 );
		map10.put(8 , 3.35 );
		map10.put(9,  3.14 );
		map10.put(10 , 2.98);
		map10.put(15 , 2.54);
		map10.put(20 , 2.35);
		map10.put(30 , 2.16);
		map10.put(40 , 2.08 );
		map10.put(60 , 1.99 );
		map10.put(120 ,1.91 );

		map.put(10, map10);

		//m = 15
	map15.put(1, 245.9 );
		map15.put(2 , 19.43 ) ;
		map15.put(3 , 8.70 );
		map15.put(4 , 5.86 );
		map15.put(5 , 4.62 );
		map15.put(6 , 3.94 );
		map15.put(7 , 3.51 );
		map15.put(8 , 3.22 );
		map15.put(9,  3.01 );
		map15.put(10 , 2.85);
		map15.put(15 , 2.40);
		map15.put(20 , 2.20);
		map15.put(30 , 2.01);
		map15.put(40 , 1.92 );
		map15.put(60 , 1.84 );
		map15.put(120 , 1.75);

		map.put(15, map15);

		//m = 20
	map20.put(1, 248.0  );
		map20.put(2 , 19.45 ) ;
		map20.put(3 , 8.66 );
		map20.put(4 , 5.80 );
		map20.put(5 , 4.56 );
		map20.put(6 , 3.87 );
		map20.put(7 , 3.44 );
		map20.put(8 , 3.15 );
		map20.put(9,  2.94 );
		map20.put(10 , 2.77);
		map20.put(15 , 2.33);
		map20.put(20 , 2.12);
		map20.put(30 , 1.93);
		map20.put(40 , 1.84 );
		map20.put(60 , 1.75 );
		map20.put(120 , 1.66);

		map.put(20, map20);
	
		//m = 30
	map30.put(1, 250.1  );
		map30.put(2 , 19.46 ) ;
		map30.put(3 , 8.62 );
		map30.put(4 , 5.75 );
		map30.put(5 , 4.5 );
		map30.put(6 , 3.81 );
		map30.put(7 , 3.38 );
		map30.put(8 , 3.08 );
		map30.put(9,  2.86 );
		map30.put(10 ,2.7 );
		map30.put(15 ,2.25 );
		map30.put(20 ,2.04 );
		map30.put(30 ,1.84 );
		map30.put(40 ,1.74  );
		map30.put(60 ,1.65  );
		map30.put(120,1.55 );

		map.put(30, map30);

		//m = 40
	map40.put(1, 251.1  );
		map40.put(2 , 19.47 ) ;
		map40.put(3 , 8.59 );
		map40.put(4 , 5.72 );
		map40.put(5 , 4.46 );
		map40.put(6 , 3.77 );
		map40.put(7 , 3.34 );
		map40.put(8 , 3.04 );
		map40.put(9,  2.83 );
		map40.put(10 ,2.66 );
		map40.put(15 ,2.20 );
		map40.put(20 ,1.99 );
		map40.put(30 ,1.79 );
		map40.put(40 ,1.69  );
		map40.put(60 ,1.59  );
		map40.put(120,1.50 );

		map.put(40, map40);

		//m = 60
	map60.put(1,  252.2 );
		map60.put(2 ,  19.48) ;
		map60.put(3 , 8.57 );
		map60.put(4 , 5.69 );
		map60.put(5 , 4.43 );
		map60.put(6 , 3.74 );
		map60.put(7 , 3.30 );
		map60.put(8 , 3.01 );
		map60.put(9,  2.79 );
		map60.put(10 , 2.62);
		map60.put(15 , 2.16);
		map60.put(20 , 1.95);
		map60.put(30 , 1.74);
		map60.put(40 , 1.64 );
		map60.put(60 , 1.53 );
		map60.put(120 ,1.43 );

		map.put(60, map60);

		//m = 120
	map120.put(1, 253.3  );
		map120.put(2 , 19.49 ) ;
		map120.put(3 , 8.55 );
		map120.put(4 , 5.66 );
		map120.put(5 , 4.40 );
		map120.put(6 , 3.70 );
		map120.put(7 , 3.27 );
		map120.put(8 , 2.97 );
		map120.put(9,  2.75 );
		map120.put(10 , 2.58);
		map120.put(15 , 2.11);
		map120.put(20 , 1.90);
		map120.put(30 , 1.68);
		map120.put(40 , 1.58 );
		map120.put(60 , 1.47);
		map120.put(120 , 1.35);

		map.put(120, map120);



		


	}

	public double getValorCritico(int grausLiberdadeEntreAmostras, int grausLiberdadeErro){

		double vc = 0;

		Map<Integer, Double> x = new HashMap<Integer, Double>();

		x = map.get(grausLiberdadeEntreAmostras);

		int chave = 0;
		int chaveOld = 0;
		int prevGL = 0;
		int proxGL = 0;
		for (Map.Entry<Integer, Double> el : x.entrySet()){
			chave = el.getKey();
			if (chave > grausLiberdadeErro){
				proxGL = chave;
				prevGL = chaveOld;
				break;
			}
			chaveOld = chave;
		}

		if (prevGL == 0) prevGL++;
		if (proxGL > 120) proxGL = 120;
		
		try {
			vc = x.get(grausLiberdadeErro);
		}
		catch(NullPointerException npe){

			vc = (x.get(prevGL) + x.get(proxGL)) / (double) 2;
		}

		return vc;

	}

	/*
	public static void main (String args[]){

		Scanner sc = new Scanner(System.in);

		TabelaDistribuicao095f t = new TabelaDistribuicao095f();
		double valorCritico = 0;
		int glAmostras = 0;
		int glErro = 0;

		System.out.println("Digite graus liberdade amostras:");
		try {
			glAmostras = Integer.parseInt(sc.next());
		}
		catch(Exception ex){
		}

		System.out.println("Digite graus Liberdade Erro:");
		try {
			glErro = Integer.parseInt(sc.next());
		}
		catch(Exception ex){
		
		}

		valorCritico = t.getValorCritico(glAmostras, glErro);
		System.out.println(valorCritico);

	}
	*/


}

