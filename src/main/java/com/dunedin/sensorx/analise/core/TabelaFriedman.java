package com.dunedin.sensorx.analise.core;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class TabelaFriedman {

	double vc[][][];
	Map<String, Integer> map = null;

	public TabelaFriedman(){

		map = new HashMap<String, Integer>();
		map.put("0.5"  , 0);
		map.put("0.2"  , 1);
		map.put("0.1"  , 2);
		map.put("0.05" , 3);
		map.put("0.02" , 4);
		map.put("0.01" , 5);
		map.put("0.005", 6);
		map.put("0.002", 7);
		map.put("0.001", 8);

		vc = new double[7][16][10];

		vc[3][2][0] = 3.000;
		vc[3][3][1] = 4.000;

		vc[3][3][0] = 2.667;
		vc[3][3][1] = 4.667;
		vc[3][3][2] = 6; //Kendall
		vc[3][3][3] = 6;

		vc[3][4][0] = 2;
		vc[3][4][1] = 4.5;
		vc[3][4][2] = 6;
		vc[3][4][3] = 6.5;
		vc[3][4][4] = 8; //Kendall
		vc[3][4][5] = 8; //Kendall
		vc[3][4][6] = 8;		

		vc[3][5][0] = 2.8;
		vc[3][5][1] = 3.6;
		vc[3][5][2] = 5.2;
		vc[3][5][3] = 6.4;
		vc[3][5][4] = 8.4;
		vc[3][5][5] = 8.4;
		vc[3][5][6] = 10; 
		vc[3][5][7] = 10; //Kendall
		vc[3][5][8] = 10;

		vc[3][6][0] = 2.33;
		vc[3][6][1] = 4;
		vc[3][6][2] = 5.33;
		vc[3][6][3] = 7;
		vc[3][6][4] = 8.33;
		vc[3][6][5] = 9;
		vc[3][6][6] = 10.33;
		vc[3][6][7] = 10.33;
		vc[3][6][8] = 12;

		vc[3][7][0] = 2;
		vc[3][7][1] = 3.714;
		vc[3][7][2] = 5.429;
		vc[3][7][3] = 7.143;
		vc[3][7][4] = 8;
		vc[3][7][5] = 8.857;
		vc[3][7][6] = 10.286;
		vc[3][7][7] = 11.143;
		vc[3][7][8] = 12.286;

		vc[3][8][0] = 2.25;
		vc[3][8][1] = 4;
		vc[3][8][2] = 5.25;
		vc[3][8][3] = 6.25;
		vc[3][8][4] = 7.75;
		vc[3][8][5] = 9;
		vc[3][8][6] = 9.75;
		vc[3][8][7] = 12;
		vc[3][8][8] = 12.25;
		
		vc[3][9][0] = 2;
		vc[3][9][1] = 3.556;
		vc[3][9][2] = 5.556;
		vc[3][9][3] = 6.222;
		vc[3][9][4] = 8;
		vc[3][9][5] = 9.556;
		vc[3][9][6] = 10.667;
		vc[3][9][7] = 11.556;
		vc[3][9][8] = 12.667;

		vc[3][10][0] = 1.8;
		vc[3][10][1] = 3.8;
		vc[3][10][2] = 5;
		vc[3][10][3] = 6.2;
		vc[3][10][4] = 7.8;
		vc[3][10][5] = 9.6;
		vc[3][10][6] = 10.4;
		vc[3][10][7] = 12.2;
		vc[3][10][8] = 12.6;

		vc[3][11][0] = 4.636;
		vc[3][11][1] = 3.818;
		vc[3][11][2] = 4.909;
		vc[3][11][3] = 6.545;
		vc[3][11][4] = 7.818;
		vc[3][11][5] = 9.455;
		vc[3][11][6] = 10.364;
		vc[3][11][7] = 11.636;
		vc[3][11][8] = 13.273;

		vc[3][12][0] = 1.5;
		vc[3][12][1] = 3.5;
		vc[3][12][2] = 5.167;
		vc[3][12][3] = 6.167;
		vc[3][12][4] = 8;
		vc[3][12][5] = 9.5;
		vc[3][12][6] = 10.167;
		vc[3][12][7] = 12.167;
		vc[3][12][8] = 12.5;

		vc[3][13][0] = 1.846;
		vc[3][13][1] = 3.846;
		vc[3][13][2] = 4.769;
		vc[3][13][3] = 6;
		vc[3][13][4] = 8;
		vc[3][13][5] = 9.385;
		vc[3][13][6] = 10.308;
		vc[3][13][7] = 11.538;
		vc[3][13][8] = 12.923;

		vc[3][14][0] = 1.714;
		vc[3][14][1] = 3.571;
		vc[3][14][2] = 5.143;
		vc[3][14][3] = 6.143;
		vc[3][14][4] = 8.143;
		vc[3][14][5] = 9;
		vc[3][14][6] = 10.429;
		vc[3][14][7] = 12;
		vc[3][14][8] = 13.286;

		vc[3][15][0] = 1.733;
		vc[3][15][1] = 3.6;
		vc[3][15][2] = 4.933;
		vc[3][15][3] = 6.4;
		vc[3][15][4] = 8.133;
		vc[3][15][5] = 8.933;
		vc[3][15][6] = 10;
		vc[3][15][7] = 12.133;
		vc[3][15][8] = 12.933;

		vc[4][2][0] = 3.6;
		vc[4][2][1] = 5.4;
		vc[4][2][2] = 6;
		vc[4][2][3] = 6;
		
		vc[4][3][0] = 3.4;
		vc[4][3][1] = 5.4;
		vc[4][3][2] = 6.6;
		vc[4][3][3] = 7.4;
		vc[4][3][4] = 8.2;
		vc[4][3][5] = 9;
		vc[4][3][6] = 9;
		vc[4][3][7] = 9;
		//vc[4][][8] = ;		

		vc[4][4][0] = 3;
		vc[4][4][1] = 4.8;
		vc[4][4][2] = 6.3;
		vc[4][4][3] = 7.8;
		vc[4][4][4] = 8.4;
		vc[4][4][5] = 9.6;
		vc[4][4][6] = 10.2;
		vc[4][4][7] = 10.2;
		vc[4][4][8] = 11.1;

		vc[4][5][0] = 3;
		vc[4][5][1] = 5.16;
		vc[4][5][2] = 6.36;
		vc[4][5][3] = 7.8;
		vc[4][5][4] = 9.24;
		vc[4][5][5] = 9.96;
		vc[4][5][6] = 10.92;
		vc[4][5][7] = 11.64;
		vc[4][5][8] = 12.6;

		vc[4][6][0] = 3;
		vc[4][6][1] = 4.8;
		vc[4][6][2] = 6.4;
		vc[4][6][3] = 7.6;
		vc[4][6][4] = 9.4;
		vc[4][6][5] = 10.2;
		vc[4][6][6] = 11.4;
		vc[4][6][7] = 12.2;
		vc[4][6][8] = 12.8;

		vc[4][7][0] = 2.829;
		vc[4][7][1] = 4.886;
		vc[4][7][2] = 6.429;
		vc[4][7][3] = 7.8;
		vc[4][7][4] = 9.343;
		vc[4][7][5] = 10.371;
		vc[4][7][6] = 11.4;
		vc[4][7][7] = 12.771;
		vc[4][7][8] = 13.8;

		vc[4][8][0] = 2.55;
		vc[4][8][1] = 4.8;
		vc[4][8][2] = 6.3;
		vc[4][8][3] = 7.65;
		vc[4][8][4] = 9.45;
		vc[4][8][5] = 10.35;
		vc[4][8][6] = 11.85;
		vc[4][8][7] = 12.9;
		vc[4][8][8] = 13.8;

		vc[4][9][2] = 6.467;
		vc[4][9][3] = 7.8;
		vc[4][9][4] = 9.133;
		vc[4][9][5] = 10.867;
		vc[4][9][6] = 12.067;
		vc[4][9][8] = 14.467;

		vc[4][10][2] = 6.36;
		vc[4][10][3] = 7.8;
		vc[4][10][4] = 9.12;
		vc[4][10][5] = 10.8;
		vc[4][10][6] = 12;
		vc[4][10][8] = 14.64;

		vc[4][11][2] = 6.382;
		vc[4][11][3] = 7.909;
		vc[4][11][4] = 9.327;
		vc[4][11][5] = 11.073;
		vc[4][11][6] = 12.273;
		vc[4][11][8] = 14.891;

		vc[4][12][2] = 6.4;
		vc[4][12][3] = 7.9;
		vc[4][12][4] = 9.2;
		vc[4][12][5] = 11.1;
		vc[4][12][6] = 12.3;
		vc[4][12][8] = 15;

		vc[4][13][2] = 6.415;
		vc[4][13][3] = 7.985;
		vc[4][13][4] = 7.369;
		vc[4][13][5] = 11.123;
		vc[4][13][6] = 12.323;
		vc[4][13][8] = 15.277;

		vc[4][14][2] = 6.343;
		vc[4][14][3] = 7.886;
		vc[4][14][4] = 9.343;
		vc[4][14][5] = 11.143;
		vc[4][14][6] = 12.514;
		vc[4][14][8] = 15.257;

		vc[4][15][2] = 6.44;
		vc[4][15][3] = 8.04;
		vc[4][15][4] = 9.4;
		vc[4][15][5] = 11.24;
		vc[4][15][6] = 12.52;
		vc[4][15][8] = 15.4;

		vc[5][2][2] = 7.2;
		vc[5][2][3] = 7.6;
		vc[5][2][4] = 8;
		vc[5][2][5] = 8;

		vc[5][3][2] = 7.467;
		vc[5][3][3] = 8.533;
		vc[5][3][4] = 9.6;
		vc[5][3][5] = 10.133;
		vc[5][3][6] = 10.667;
		vc[5][3][8] = 11.467;

		vc[5][4][2] = 7.6;
		vc[5][4][3] = 8.8;
		vc[5][4][4] = 9.8;
		vc[5][4][5] = 11.2;
		vc[5][4][6] = 12;
		vc[5][4][8] = 13.2;

		vc[5][5][2] = 7.68;
		vc[5][5][3] = 8.96;
		vc[5][5][4] = 10.24;
		vc[5][5][5] = 11.68;
		vc[5][5][6] = 12.48;
		vc[5][5][8] = 14.4;

		vc[5][6][2] = 7.733;
		vc[5][6][3] = 9.067;
		vc[5][6][4] = 10.4;
		vc[5][6][5] = 11.867;
		vc[5][6][6] = 13.067;
		vc[5][6][8] = 15.2;

		vc[5][7][2] = 7.771;
		vc[5][7][3] = 9.143;
		vc[5][7][4] = 10.514;
		vc[5][7][5] = 12.114;
		vc[5][7][6] = 13.257;
		vc[5][7][8] = 15.657;

		vc[5][8][2] = 7.8;
		vc[5][8][3] = 9.3;
		vc[5][8][4] = 10.6;
		vc[5][8][5] = 12.3;
		vc[5][8][6] = 13.5;
		vc[5][8][8] = 16;

		vc[5][9][2] = 7.733;
		vc[5][9][3] = 9.244;
		vc[5][9][4] = 10.667;
		vc[5][9][5] = 12.444;
		vc[5][9][6] = 13.689;
		vc[5][9][8] = 16.356;

		vc[5][10][2] = 7.76;
		vc[5][10][3] = 9.28;
		vc[5][10][4] = 10.72;
		vc[5][10][5] = 12.48;
		vc[5][10][6] = 13.84;
		vc[5][10][8] = 16.48;

		vc[6][2][2] = 8.286;
		vc[6][2][3] = 9.143;
		vc[6][2][4] = 9.429;
		vc[6][2][5] = 9.714;
		vc[6][2][6] = 10;
		//vc[6][2][8] = ;

		vc[6][3][2] = 8.714;
		vc[6][3][3] = 9.857;
		vc[6][3][4] = 10.81;
		vc[6][3][5] = 11.762;
		vc[6][3][6] = 12.524;
		vc[6][3][8] = 13.286;

		vc[6][4][2] = 9;
		vc[6][4][3] = 10.286;
		vc[6][4][4] = 11.429;
		vc[6][4][5] = 12.714;
		vc[6][4][6] = 13.571;
		vc[6][4][8] = 15.286;

		vc[6][5][2] = 9;
		vc[6][5][3] = 10.486;
		vc[6][5][4] = 11.743;
		vc[6][5][5] = 13.229;
		vc[6][5][6] = 14.257;
		vc[6][5][8] = 16.429;

		vc[6][6][2] = 9.048;
		vc[6][6][3] = 10.571;
		vc[6][6][4] = 12;
		vc[6][6][5] = 13.619;
		vc[6][6][6] = 14.762;
		vc[6][6][8] = 17.048;

		vc[6][7][2] = 9.122;
		vc[6][7][3] = 10.674;
		vc[6][7][4] = 12.061;
		vc[6][7][5] = 13.857;
		vc[6][7][6] = 15;
		vc[6][7][8] = 17.612;

		vc[6][8][2] = 9.143;
		vc[6][8][3] = 10.714;
		vc[6][8][4] = 12.214;
		vc[6][8][5] = 14;
		vc[6][8][6] = 15.286;
		vc[6][8][8] = 18;

		vc[6][9][2] = 9.127;
		vc[6][9][3] = 10.778;
		vc[6][9][4] = 12.302;
		vc[6][9][5] = 14.143;
		vc[6][9][6] = 15.476;
		vc[6][9][8] = 18.270;

		vc[6][10][2] = 9.143;
		vc[6][10][3] = 10.8;
		vc[6][10][4] = 12.343;
		vc[6][10][5] = 14.299;
		vc[6][10][6] = 15.6;
		vc[6][10][8] = 18.514;		





	}

	public double getValorCritico(int a, int b, String alfa){

		//Map<Double, Integer> map = null;

		int pos = map.get(alfa);

		return vc[a][b][pos];

	}

	/*
	public static void main (String args[]){

		TabelaFriedman t = new TabelaFriedman();
		double valorCritico = 0;
		Scanner sc = new Scanner(System.in);
		int a = 0;
		int b = 0;
		//double alfa = 0;
		String alfa = "";

		System.out.printf("Tabela de Valores Criticos de Friedman\n\n");

		System.out.println("Digite a:");
		try {
			a = Integer.parseInt(sc.next());
		}
		catch(Exception ex){
		}

		System.out.println("Digite b:");
		try {
			b = Integer.parseInt(sc.next());
		}
		catch(Exception ex){
		}

		System.out.println("Digite quantil:");
		try {
			//alfa = Double.parseDouble(sc.next());
			alfa = sc.next();
		}
		catch(Exception ex){
		}



		try {

			valorCritico = t.getValorCritico(a, b, alfa);

		}
		catch(Exception ex){
			ex.printStackTrace();
			//System.out.println("Valor nao encontrado");
		}

		System.out.println(valorCritico);

	}
	*/

}

