package com.dunedin.sensorx.analise.core;

import java.util.HashMap;
import java.util.Map;

public class TabelatStudent {

	//Map<Double, Map<Double, Double>> map = new HashMap<Double, Map<Double, Double>>();
	Map<Integer, Map<Double, Double>> map = new HashMap<>();

		// p = .55
	//Map<Double, Double> map55 = new HashMap<Double, Double>();
	Map<Double, Double> map1 = new HashMap<>();
	Map<Double, Double> map2 = new HashMap<>();
	Map<Double, Double> map3 = new HashMap<>();
	Map<Double, Double> map4 = new HashMap<>();
	Map<Double, Double> map5 = new HashMap<>();
	Map<Double, Double> map6 = new HashMap<>();
	Map<Double, Double> map7 = new HashMap<>();
	Map<Double, Double> map8 = new HashMap<>();
	Map<Double, Double> map9 = new HashMap<>();
	Map<Double, Double> map10 = new HashMap<>();
	Map<Double, Double> map11 = new HashMap<>();
	Map<Double, Double> map12 = new HashMap<>();
	Map<Double, Double> map13 = new HashMap<>();
	Map<Double, Double> map14 = new HashMap<>();
	Map<Double, Double> map15 = new HashMap<>();
	Map<Double, Double> map16 = new HashMap<>();
	Map<Double, Double> map17 = new HashMap<>();
	Map<Double, Double> map18 = new HashMap<>();
	Map<Double, Double> map19 = new HashMap<>();
	Map<Double, Double> map20 = new HashMap<>();
	Map<Double, Double> map21 = new HashMap<>();
	Map<Double, Double> map22 = new HashMap<>();
	Map<Double, Double> map23 = new HashMap<>();
	Map<Double, Double> map24 = new HashMap<>();
	Map<Double, Double> map25 = new HashMap<>();
	Map<Double, Double> map26 = new HashMap<>();
	Map<Double, Double> map27 = new HashMap<>();
	Map<Double, Double> map28 = new HashMap<>();
	Map<Double, Double> map29 = new HashMap<>();
	Map<Double, Double> map30 = new HashMap<>();
	Map<Double, Double> map40 = new HashMap<>();
	Map<Double, Double> map60 = new HashMap<>();
	Map<Double, Double> map120 = new HashMap<>();


	public TabelatStudent(){

		//m = 1
		map1.put(.550, .158  );
		map1.put(.600, .325 ) ;
		map1.put(.650, .510  );
		map1.put(.700, .727  );
		map1.put(.750, 1.0  );
		map1.put(.800, 1.376  );
		map1.put(.850, 1.963 );
		map1.put(.900, 3.078 );
		map1.put(.950, 6.314 );
		map1.put(.975, 12.706 );
		map1.put(.990, 31.821 );
		map1.put(.995, 63.657 );

		map.put(1, map1);

		//m = 2
		map2.put(.550, .142  );
		map2.put(.600, .289 ) ;
		map2.put(.650, .445  );
		map2.put(.700, .617  );
		map2.put(.750, .816  );
		map2.put(.800, 1.061  );
		map2.put(.850, 1.386 );
		map2.put(.900, 1.886 );
		map2.put(.950, 2.92 );
		map2.put(.975, 4.303 );
		map2.put(.990, 6.965 );
		map2.put(.995, 9.925 );

		map.put(2 , map2);

			//m = 3
		map3.put(.550, .137  );
		map3.put(.600, .277 ) ;
		map3.put(.650, .424  );
		map3.put(.700, .584  );
		map3.put(.750, .765  );
		map3.put(.800, .978  );
		map3.put(.850, 1.25 );
		map3.put(.900, 1.638 );
		map3.put(.950, 2.353 );
		map3.put(.975, 3.182 );
		map3.put(.990, 4.541 );
		map3.put(.995, 5.841);

		map.put(3 , map3);

				//m = 4
		map4.put(.550, .134  );
		map4.put(.600, .271 ) ;
		map4.put(.650, .414  );
		map4.put(.700, .569  );
		map4.put(.750, .741  );
		map4.put(.800, .941  );
		map4.put(.850, 1.19 );
		map4.put(.900, 1.533 );
		map4.put(.950, 2.132 );
		map4.put(.975, 2.776 );
		map4.put(.990, 3.747 );
		map4.put(.995, 4.604 );

		map.put(4 , map4);

				//m = 5
		map5.put(.550, .132  );
		map5.put(.600, .267 ) ;
		map5.put(.650, .408  );
		map5.put(.700, .559  );
		map5.put(.750, .727  );
		map5.put(.800, .92  );
		map5.put(.850, 1.156 );
		map5.put(.900, 1.476 );
		map5.put(.950, 2.015 );
		map5.put(.975, 2.571 );
		map5.put(.990, 3.365 );
		map5.put(.995, 4.032 );

		map.put(5 , map5);

				//m = 6
		map6.put(.550, .131  );
		map6.put(.600, .265 ) ;
		map6.put(.650, .404  );
		map6.put(.700, .553  );
		map6.put(.750, .718  );
		map6.put(.800, .906  );
		map6.put(.850, 1.134 );
		map6.put(.900, 1.44 );
		map6.put(.950, 1.943 );
		map6.put(.975, 2.447 );
		map6.put(.990, 3.143 );
		map6.put(.995, 3.707 );

		map.put(6, map6);

				//m = 7
		map7.put(.550, .13  );
		map7.put(.600, .263 ) ;
		map7.put(.650, .402  );
		map7.put(.700, .549  );
		map7.put(.750, .711  );
		map7.put(.800, .896  );
		map7.put(.850, 1.119 );
		map7.put(.900, 1.415 );
		map7.put(.950, 1.895 );
		map7.put(.975, 2.365 );
		map7.put(.990, 2.998 );
		map7.put(.995, 3.499 );

		map.put(7 , map7);

				//m = 8
		map8.put(.550,  .13 );
		map8.put(.600,  .262) ;
		map8.put(.650,  .399 );
		map8.put(.700,  .546 );
		map8.put(.750,  .706 );
		map8.put(.800,  .889 );
		map8.put(.850,  1.108);
		map8.put(.900,  1.397);
		map8.put(.950,  1.86);
		map8.put(.975,  2.306);
		map8.put(.990,  2.896);
		map8.put(.995,  3.355);

		map.put(8 , map8);
		
		//m = 9
		map9.put(.550,  .129  );
		map9.put(.600,  .261) ;
		map9.put(.650,  .398 );
		map9.put(.700,  .543 );
		map9.put(.750,  .703 );
		map9.put(.800,  .883 );
		map9.put(.850, 1.1 );
		map9.put(.900, 1.383 );
		map9.put(.950, 1.833  );
		map9.put(.975, 2.262);
		map9.put(.990, 2.821);
		map9.put(.995, 3.25);

		map.put(9, map9);



		//m = 10
		map10.put(.550, .129  );
		map10.put(.600, .26 ) ;
		map10.put(.650, .397  );
		map10.put(.700, .542  );
		map10.put(.750, .7  );
		map10.put(.800, .879  );
		map10.put(.850, 1.093 );
		map10.put(.900, 1.372 );
		map10.put(.950, 1.812 );
		map10.put(.975, 2.228 );
		map10.put(.990, 2.764 );
		map10.put(.995, 3.169 );

		map.put(10 , map10);




		//m = 11
		map11.put(.550, .129  );
		map11.put(.600, .26 ) ;
		map11.put(.650, .396  );
		map11.put(.700, .54  );
		map11.put(.750, .697  );
		map11.put(.800, .876  );
		map11.put(.850, 1.088 );
		map11.put(.900, 1.363 );
		map11.put(.950, 1.796 );
		map11.put(.975, 2.201 );
		map11.put(.990, 2.718 );
		map11.put(.995, 3.106 );

		map.put(11 , map11);




		//m = 12
		map12.put(.550, .128  );
		map12.put(.600, .259 ) ;
		map12.put(.650, .395  );
		map12.put(.700, .539  );
		map12.put(.750, .695  );
		map12.put(.800, .873  );
		map12.put(.850, 1.083 );
		map12.put(.900, 1.356 );
		map12.put(.950, 1.782 );
		map12.put(.975, 2.179 );
		map12.put(.990, 2.681 );
		map12.put(.995, 3.055 );

		map.put(12 , map12);




		//m = 13
		map13.put(.550,  .128 );
		map13.put(.600,  .259) ;
		map13.put(.650,  .394 );
		map13.put(.700,  .538 );
		map13.put(.750,  .694 );
		map13.put(.800,  .87 );
		map13.put(.850,  1.079);
		map13.put(.900,  1.35);
		map13.put(.950,  1.771);
		map13.put(.975,  2.16);
		map13.put(.990,  2.65);
		map13.put(.995,  3.012);

		map.put(13 , map13);



		//m = 14
		map14.put(.550, .128  );
		map14.put(.600, .258 ) ;
		map14.put(.650, .393  );
		map14.put(.700, .537  );
		map14.put(.750, .692  );
		map14.put(.800, .868  );
		map14.put(.850, 1.076 );
		map14.put(.900, 1.345 );
		map14.put(.950, 1.761 );
		map14.put(.975, 2.145 );
		map14.put(.990, 2.624 );
		map14.put(.995, 2.977 );

		map.put(14 , map14);



		//m = 15
		map15.put(.550, .128  );
		map15.put(.600, .258 ) ;
		map15.put(.650, .393  );
		map15.put(.700, .536  );
		map15.put(.750, .691  );
		map15.put(.800, .866  );
		map15.put(.850, 1.074 );
		map15.put(.900, 1.341 );
		map15.put(.950, 1.753 );
		map15.put(.975, 2.131 );
		map15.put(.990, 2.602 );
		map15.put(.995, 2.947 );

		map.put(15 , map15);




		//m = 16
		map16.put(.550, .128  );
		map16.put(.600, .258 ) ;
		map16.put(.650, .392  );
		map16.put(.700, .535  );
		map16.put(.750, .69  );
		map16.put(.800, .865  );
		map16.put(.850, 1.071 );
		map16.put(.900, 1.337 );
		map16.put(.950, 1.746 );
		map16.put(.975, 2.12 );
		map16.put(.990, 2.583 );
		map16.put(.995, 2.921 );

		map.put(16 , map16);



		//m = 17
		map17.put(.550, .128  );
		map17.put(.600, .257 ) ;
		map17.put(.650, .392  );
		map17.put(.700, .534  );
		map17.put(.750, .689  );
		map17.put(.800, .863  );
		map17.put(.850, 1.069 );
		map17.put(.900, 1.333 );
		map17.put(.950, 1.74 );
		map17.put(.975, 2.11 );
		map17.put(.990, 2.567 );
		map17.put(.995, 2.898 );

		map.put(17 , map17);




		//m = 18
		map18.put(.550, .127  );
		map18.put(.600, .257 ) ;
		map18.put(.650, .392  );
		map18.put(.700, .534  );
		map18.put(.750, .688  );
		map18.put(.800, .862  );
		map18.put(.850, 1.067 );
		map18.put(.900, 1.33 );
		map18.put(.950, 1.734 );
		map18.put(.975, 2.101 );
		map18.put(.990, 2.552 );
		map18.put(.995, 2.878 );

		map.put(18 , map18);




		//m = 19
		map19.put(.550, .127  );
		map19.put(.600, .257 ) ;
		map19.put(.650, .391  );
		map19.put(.700, .533  );
		map19.put(.750, .688  );
		map19.put(.800, .861  );
		map19.put(.850, 1.066 );
		map19.put(.900, 1.328 );
		map19.put(.950, 1.729 );
		map19.put(.975, 2.093 );
		map19.put(.990, 2.539 );
		map19.put(.995, 2.861 );

		map.put(19 , map19);




		//m = 20
		map20.put(.550, .127  );
		map20.put(.600, .257 ) ;
		map20.put(.650, .391  );
		map20.put(.700, .533  );
		map20.put(.750, .687  );
		map20.put(.800, .86  );
		map20.put(.850, 1.064 );
		map20.put(.900, 1.325 );
		map20.put(.950, 1.725 );
		map20.put(.975, 2.086 );
		map20.put(.990, 2.528 );
		map20.put(.995, 2.845 );

		map.put(20 , map20);




		//m = 21
		map21.put(.550, .127  );
		map21.put(.600, .257 ) ;
		map21.put(.650, .391  );
		map21.put(.700, .532  );
		map21.put(.750, .686  );
		map21.put(.800, .859  );
		map21.put(.850, 1.063 );
		map21.put(.900, 1.323 );
		map21.put(.950, 1.721 );
		map21.put(.975, 2.080 );
		map21.put(.990, 2.518 );
		map21.put(.995, 2.831 );

		map.put(21 , map21);




		//m = 22
		map22.put(.550, .127  );
		map22.put(.600, .256 ) ;
		map22.put(.650, .39  );
		map22.put(.700, .532  );
		map22.put(.750, .686  );
		map22.put(.800, .858  );
		map22.put(.850, 1.061 );
		map22.put(.900, 1.321 );
		map22.put(.950, 1.717 );
		map22.put(.975, 2.074 );
		map22.put(.990, 2.508 );
		map22.put(.995, 2.819 );

		map.put(22 , map22);




		//m = 23
		map23.put(.550, .127  );
		map23.put(.600, .256 ) ;
		map23.put(.650, .39  );
		map23.put(.700, .532  );
		map23.put(.750, .685  );
		map23.put(.800, .858  );
		map23.put(.850, 1.060 );
		map23.put(.900, 1.319 );
		map23.put(.950, 1.714 );
		map23.put(.975, 2.069 );
		map23.put(.990, 2.5 );
		map23.put(.995, 2.807 );

		map.put(23 , map23);




		//m = 24
		map24.put(.550, .127  );
		map24.put(.600, .256 ) ;
		map24.put(.650, .39  );
		map24.put(.700, .531  );
		map24.put(.750, .685  );
		map24.put(.800, .857  );
		map24.put(.850, 1.059 );
		map24.put(.900, 1.318 );
		map24.put(.950, 1.711 );
		map24.put(.975, 2.064 );
		map24.put(.990, 2.492 );
		map24.put(.995, 2.797 );

		map.put(24 , map24);




		//m = 25
		map25.put(.550, .127  );
		map25.put(.600, .256 ) ;
		map25.put(.650, .39  );
		map25.put(.700, .531  );
		map25.put(.750, .684  );
		map25.put(.800, .856  );
		map25.put(.850, 1.058 );
		map25.put(.900, 1.316 );
		map25.put(.950, 1.708 );
		map25.put(.975, 2.060 );
		map25.put(.990, 2.485 );
		map25.put(.995, 2.787 );

		map.put(25 , map25);




		//m = 26
		map26.put(.550,  .127 );
		map26.put(.600,  .256) ;
		map26.put(.650,  .39 );
		map26.put(.700,  .531 );
		map26.put(.750,  .684 );
		map26.put(.800,  .856 );
		map26.put(.850,  1.058);
		map26.put(.900,  1.315);
		map26.put(.950,  1.706);
		map26.put(.975,  2.056);
		map26.put(.990,  2.479);
		map26.put(.995,  2.779);

		map.put(26 , map26);




		//m = 27
		map27.put(.550, .127  );
		map27.put(.600, .256 ) ;
		map27.put(.650, .389  );
		map27.put(.700, .531  );
		map27.put(.750, .684  );
		map27.put(.800, .855  );
		map27.put(.850, 1.057 );
		map27.put(.900, 1.314 );
		map27.put(.950, 1.703 );
		map27.put(.975, 2.052 );
		map27.put(.990, 2.473 );
		map27.put(.995, 2.771 );

		map.put(27 , map27);




		//m = 28
		map28.put(.550,  .127 );
		map28.put(.600, .256 ) ;
		map28.put(.650, .389  );
		map28.put(.700, .53  );
		map28.put(.750, .683  );
		map28.put(.800, .855  );
		map28.put(.850, 1.056 );
		map28.put(.900, 1.313 );
		map28.put(.950, 1.701 );
		map28.put(.975, 2.048 );
		map28.put(.990, 2.467 );
		map28.put(.995, 2.763 );

		map.put(28 , map28);




		//m = 29
		map29.put(.550, .127  );
		map29.put(.600, .256 ) ;
		map29.put(.650, .389  );
		map29.put(.700, .53  );
		map29.put(.750, .683  );
		map29.put(.800, .854  );
		map29.put(.850, 1.055 );
		map29.put(.900, 1.311 );
		map29.put(.950, 1.699 );
		map29.put(.975, 2.045 );
		map29.put(.990, 2.462 );
		map29.put(.995, 2.756 );

		map.put(29 , map29);




		//m = 30
		map30.put(.550, .127  );
		map30.put(.600, .256 ) ;
		map30.put(.650, .389  );
		map30.put(.700, .53  );
		map30.put(.750, .683  );
		map30.put(.800, .854  );
		map30.put(.850, 1.055 );
		map30.put(.900, 1.31 );
		map30.put(.950, 1.697 );
		map30.put(.975, 2.042 );
		map30.put(.990, 2.457 );
		map30.put(.995, 2.75 );

		map.put(30 , map30);




		//m = 40
		map40.put(.550, .126  );
		map40.put(.600, .255 ) ;
		map40.put(.650, .388  );
		map40.put(.700, .529  );
		map40.put(.750, .681  );
		map40.put(.800, .851  );
		map40.put(.850, 1.05 );
		map40.put(.900, 1.303 );
		map40.put(.950, 1.684 );
		map40.put(.975, 2.021 );
		map40.put(.990, 2.423 );
		map40.put(.995, 2.704 );

		map.put(40 , map40);




		//m = 60
		map60.put(.550, .126  );
		map60.put(.600, .254 ) ;
		map60.put(.650, .387  );
		map60.put(.700, .527  );
		map60.put(.750, .679  );
		map60.put(.800, .848  );
		map60.put(.850, 1.046 );
		map60.put(.900, 1.296 );
		map60.put(.950, 1.671 );
		map60.put(.975, 2.0 );
		map60.put(.990, 2.39 );
		map60.put(.995, 2.66 );

		map.put(60 , map60);




		//m = 120
		map120.put(.550, .126  );
		map120.put(.600, .254 ) ;
		map120.put(.650, .386  );
		map120.put(.700, .524  );
		map120.put(.750, .674  );
		map120.put(.800, .842  );
		map120.put(.850, 1.036 );
		map120.put(.900, 1.282 );
		map120.put(.950, 1.645 );
		map120.put(.975, 1.96 );
		map120.put(.990, 2.326 );
		map120.put(.995, 2.576 );

		map.put(120 , map120);



	}

	public double getValorCritico(double icDecimal, int grausLiberdadeErro){

		double vc = 0;

		//Map<Integer, Map<Double, Double>> map = new HashMap<>();
		Map<Double, Double> x = new HashMap<Double, Double>();

		x = map.get(grausLiberdadeErro);

		double chave = 0;
		double prevIc = 0;
		double proxIc = 0;
		for (Map.Entry<Double, Double> el : x.entrySet()){
			chave = el.getKey();
			if (chave > icDecimal){
				proxIc = chave;
				break;
			}
		}
		double chaveAnterior = .55;
		for (Map.Entry<Double, Double> el : x.entrySet()){
			chave = el.getKey();
			if (chave > icDecimal){
				prevIc = chaveAnterior;
				break;
			}
			chaveAnterior = chave;
		}

		if (prevIc == 0) prevIc = .55;
		if (proxIc == 0) proxIc = .995;
		
		try {
			vc = x.get(icDecimal);
		}
		catch(NullPointerException npe){
			vc = (x.get(prevIc) + x.get(proxIc)) / (double) 2;
		}

		return vc;
	}

	/*
	public static void main (String args[]){

		TabelatStudent t = new TabelatStudent();

		double valorCritico = 0;

		try {

			valorCritico = t.getValorCritico(Double.parseDouble(args[0]), Integer.parseInt(args[1]));

		}
		catch(Exception ex){
			System.out.println("Valor nao encontrado");
		}

		System.out.printf("%.3f\n", valorCritico);

	}
	*/


}

