package com.dunedin.sensorx.analise.core;

import java.util.ArrayList;
import java.util.Vector;

public class TesteFriedman extends Amostras{

	//private String amostras[][];
	//private double chiR = 0;

	public TesteFriedman(String amostras[][]){
		setAmostras(amostras);
	}

	public double getChiR(){
		return funcao();
	}

	public double funcao(){

		int b = getQtdeProvadores(0);
		int a = getQtdeAmostras();

		return ((double) 12 / (double) (b * a * (a + 1))) * getSomaQuadradoRanks() - 3 * b * (a + 1);

	}

	public int buscarIdx(double val, double[] arr){

		int i = -1;

		for ( i=0; i < arr.length; i++)
			if (val == arr[i])
				break;
		
		return i;

	}

	public double[] ordenarArr(double arr[] ){

		int min = 0;
		double temp = 0;

		for (int i=0; i < arr.length;i++){
			min = i;
			for (int j=i+1; j < arr.length; j++)
				if (arr[j] < arr[min])
					min = j;

			temp = arr[min];
			arr[min] = arr[i];
			arr[i] = temp;	

		}
		return arr;

	}

	public double getSomaQuadradoRanks(){

		int np = getQtdeProvadores(0);
		int na = getQtdeAmostras();

		double m3D[][][] = new double[na][np][2];
		double arr[] = new double[na];
		double ord[] = new double[na];
		double subtotal  = 0;
		double soma = 0;
		double temp = 0;

		try {

			for (int j=1; j <= np; j++){
				
				for (int i=0; i < na; i++){

					m3D[i][j-1][0] = Double.parseDouble(amostras[i][j]);
					//System.out.printf("%s ", amostras[i][j]);

				}
				//System.out.println();
			}

			for (int j=0; j < m3D[0].length; j++){

				for (int i=0; i < m3D.length; i++){

					arr[i] = m3D[i][j][0];

				}

				ord = ordenarArr(arr);

				for (int i=0; i < m3D.length; i++){

					temp = m3D[i][j][0];

					m3D[i][j][1] = buscarIdx(temp, ord) + 1;
					//System.out.printf("%.2f ", temp);
					
				}	
				//System.out.println();			

			}

			for (int i=0; i < na; i++){
				for (int j=0; j < np; j++){
					subtotal += m3D[i][j][1];
				}
				soma += subtotal * subtotal;
				subtotal = 0;

			}
			//System.out.printf("Soma = %.2f", soma);

		}
		catch(Exception ex){
			ex.printStackTrace();
		}

		return soma;

	}

	/*
	public static void main(String[] args){

		TesteFriedman tf = new TesteFriedman();
		System.out.printf("Chi Residual = %.2f\n", tf.getChiR());

	}
	*/

}