package com.dunedin.sensorx.analise.core;

import java.util.Arrays;
import java.util.Scanner;
import java.util.Vector;
import java.nio.file.Paths;
import java.util.Map;
import java.util.HashMap;

public class Tukey extends Anova {

	double medias[][] = null;
	double SE = 0;
	Anova anova = null;
	Map<Integer, String> map = null;

	public Tukey(String amostras[][]){

		map = new HashMap<Integer, String>();
		map.put(0, "1");
		map.put(1, "2");
		map.put(2, "3");
		map.put(3, "4");
		map.put(4, "5");

		setAmostras(amostras);
		
		medias = new double[amostras.length][2];

		for (int i = 0; i < amostras.length; i++){

			medias[i][0] = i;
			medias[i][1] = getMedia(i);
			//System.out.println(medias[i][1]);

		}

		

	}

	public double[][] getMedias(){

		return medias;

	}

	public String getMap(int i){

		return map.get(i);

	}

	public double getDiffMedias(int i, int j){

		return Math.abs(medias[i][1] - medias[j][1]);

	}

	public double getSE(){

		//System.out.println(getErroMS(amostras));

		//System.out.println(getTamArr(amostras[0]));

		return Math.sqrt(getErroMS() / (double) amostras[0].length);

	}

	/*
	public static void main (String args[]){

		Scanner sc = new Scanner(System.in);

		double intvconf = 0;
		double qTabelado = 0;
		double fTabelado = 0;
		double qCalculado = 0;
		String msg = "";

		//Amostras am = new Amostras();
		TabelatStudent dts = new TabelatStudent();

		TabelaDistribuicao095f tabF = new TabelaDistribuicao095f();

		String amostras[][] = null;

		//amostras = getAmostras();

		//int erroDF = getErroDF();

		//System.out.println(x);

		Tukey tk = new Tukey();

		double[][] medias = tk.getMedias();

		//System.out.println(tk.getErroDF())

		//double diffMedias = tk.getDiffMedias();

		//double se = tk.getSE();

		//System.out.println("Para");

		System.out.println("                              RESUMO DA ANOVA");
		System.out.println("ORIGEM DA VARIACAO         SS               DF                  MS");
		System.out.println("------------------------------------------------------------------");
		System.out.printf("TOTAL\t\t\t%.2f\t\t%d\n", tk.getSS(), (tk.getQtdeTotalObs() - 1));
		System.out.printf(" ENTRE AMOSTRAS\t\t%.2f\t\t%d\t\t%.2f\n", tk.getGruposSS(), (tk.getQtdeAmostras() - 1), tk.getGruposMS());
		System.out.printf(" ERRO\t\t\t%.2f\t\t%d\t\t%.2f\n", tk.getErroSS(), (tk.getQtdeTotalObs() - tk.getQtdeAmostras()), tk.getErroMS());
		System.out.println("------------------------------------------------------------------");
		System.out.printf("F calculado = %.2f\n", tk.getFCalculado());
		System.out.println("F tabela = " + tabF.getValorCritico(tk.getQtdeAmostras() - 1, tk.getErroDF()));

		
		System.out.println("Para Alfa (exemplo, 0.05), ");
		System.out.println("Graus de Liberdade Entre Amostras = " + (getQtdeAmostras() - 1) + ",");
		System.out.println("Graus de Liberdade do Erro = " + getErroDF(amostras) + ",");
		System.out.println("digite F tabelado:");

		try {
			fTabelado = Double.parseDouble(sc.next());
		}
		catch(Exception ex){

		}
		

		qTabelado = dts.getValorCritico(0.90, tk.getErroDF());
		
		System.out.println("\nComp\tDiff Medias\t\t\tSE\tq calculado\tq tabelado\tConclusao");

		for (int i = 0; i < medias.length; i++){
			for (int j = 0; j < medias.length; j++){
				if (i != j) {

					qCalculado = tk.getDiffMedias(i, j) / tk.getSE();

					if (qCalculado > qTabelado){
						msg = "Rejeitar";
					}
					else {
						msg = "Aceitar";
					}		

					msg += " Hipotese Nula: Media" + (i + 1) + " = Media" + (j + 1);
						
					System.out.printf("%s vs %s\t%.2f - %.2f = %.2f\t\t%.2f\t%.2f\t\t%.3f\t\t%s\n", tk.getMap(i), tk.getMap(j), medias[i][1], medias[j][1], tk.getDiffMedias(i, j), tk.getSE(), qCalculado, qTabelado, msg);
					
				
				}

			}

		}

	}
	*/

}
