package com.dunedin.sensorx.analise.dao;

import com.dunedin.sensorx.analise.model.User;

public interface UserDao{

	/*
	public  int register(String name, String email, String password, String cPF, String gender);
	public  int updateGender(int userId, String gender);
	public User findUser(String username);
	*/
	public User findUser(String login, String password);
	//public User findUser(int userId);

}

