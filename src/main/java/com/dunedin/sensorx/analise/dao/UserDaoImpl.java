package com.dunedin.sensorx.analise.dao;

import com.dunedin.sensorx.analise.model.User;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

@Repository
public class UserDaoImpl implements UserDao {

	@PersistenceContext
	private EntityManager em;
	
	public UserDaoImpl(){};

	public User findUser(String username, String password){
		
		User u;
		
		try {
			u = (User) em.createNamedQuery("User.findUser")
						 .setParameter("username", username)
						 .setParameter("password", password)
						 .getSingleResult();
			return u;
		}
		catch(NoResultException nre){
			return null;	
		}
	}

}

