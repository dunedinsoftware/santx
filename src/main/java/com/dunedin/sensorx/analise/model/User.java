package com.dunedin.sensorx.analise.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name="USUARIO")
@NamedQueries({
    @NamedQuery(name = "User.findUser", query = "SELECT u FROM User u WHERE u.username = :username AND u.password = :password")
})
public class User implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="USUARIOID")
	private long id;

	@Column(name="USUARIONOME", length=30, nullable=false)
	private String username;

	@Column(name="SENHA", length=8, nullable=false)
	private String password;

	//private int authorisationLevel;

	public User(){
	}

	public long getId(){
		return id;
	}

	public void setId(long id){
		this.id = id;
	}

	/*
	public String getName(){
		return name;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getFamilyName(){
		return familyName;
	}

	public void setFamilyName(String familyName){
		this.familyName = familyName;
	}
	*/
	public String getUsername(){
		return username;
	}

	public void setUsername(String username){
		this.username = username;
	}

	public String getPassword(){
		return password;
	}

	public void setPassword(String password){
		this.password = password;
	}

	/*
	public String getCPF(){
		return cPF;
	}

	public void setCPF(String cPF){
		this.cPF = cPF;
	}

	public String getGender(){
		return gender;
	}

	public void setGender(String gender){
		this.gender = gender;
	}

	public int getAuthorisationLevel(){
		return authorisationLevel;
	}

	public void setAuthorisationLevel(int authorisationLevel){
		this.authorisationLevel = authorisationLevel;
	}
	*/
}

