package com.dunedin.sensorx.analise.service;

import com.dunedin.sensorx.analise.model.User;
import com.dunedin.sensorx.analise.dao.UserDao;
import com.dunedin.sensorx.analise.service.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("userService")
@Transactional
public class UserServiceImpl implements UserService{

	@Autowired
  UserDao dao;

  /*
	public int register(String name, String email, String password, String cPF, String gender){
		return dao.register(name, email, password, cPF, gender);
	}

  public int updateGender(int userId, String gender){
    return dao.updateGender(userId, gender);
  }
  
  public User findUser(String username){
    return dao.findUser(username);
  }
  */
  public User findUser(String username, String password){
    return dao.findUser(username, password);
  }

  /*
  public User findUser(int userId){
    return dao.findUser(userId);
  }
  */

}
