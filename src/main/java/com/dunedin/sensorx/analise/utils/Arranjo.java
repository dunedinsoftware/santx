package com.dunedin.sensorx.analise.utils;

public class Arranjo {

	public static double[] ordenar(double arr[] ){

		int min = 0;
		double temp = 0;

		for (int i=0; i < arr.length;i++){
			min = i;
			for (int j=i+1; j < arr.length; j++)
				if (arr[j] < arr[min])
					min = j;

			temp = arr[min];
			arr[min] = arr[i];
			arr[i] = temp;	

		}
		return arr;

	}

}