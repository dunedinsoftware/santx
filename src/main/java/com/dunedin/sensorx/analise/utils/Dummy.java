package com.dunedin.sensorx.analise.utils;

public class Dummy implements Comparable{

	private int valor = 0;

	@Override
	public int compareTo(Object outroValor){
		if ((Object)valor != outroValor)
			return 0;
		else
			return 1;
	}

	public Dummy(int i){
		this.valor = i;
	}

	public int getValor(){
		return valor;
	}

	public void setValor(int i){
		this.valor = valor;
	}

}
