package com.dunedin.sensorx.analise.utils;

public class Formatacao {

	private static int CASAS_DECIMAIS_USUARIO = 2;

	public static String br(int n){
		String ret = "";
		for (int i = 0; i < n; i++) ret += "</br>";
		return ret;
		//return "\n";
	}

	public static String espaco(int n){
		String ret = "";
		for (int i = 0; i < n; i++) ret += "-";
		return ret;
	}

	public static String hifen(int n){
		String ret = "";
		for (int i = 0; i < n; i++) ret += "-";
		return ret;
	}

	

	public static String formatar(double x){

		//System.out.println(x);
		double dec = 0;
		String ret = x + "";

		try {

			String antes = ret.substring(0, ret.indexOf("."));
			String apos  = ret.substring(ret.indexOf(".") + 1);

			if (apos.length() <= CASAS_DECIMAIS_USUARIO) return ret.replace(".", ",");

			dec = Double.parseDouble(apos.substring(0, CASAS_DECIMAIS_USUARIO) + "." + apos.substring(CASAS_DECIMAIS_USUARIO));
			ret = antes + "," + Math.round(dec);

		}
		catch(Exception e){
			e.getMessage();
		}

		return ret;

	}

	public static String formatar(double x, int casas){

		//System.out.println(x);
		double dec = 0;
		String ret = x + "";

		try {

			String antes = ret.substring(0, ret.indexOf("."));
			String apos  = ret.substring(ret.indexOf(".") + 1);

			if (apos.length() <= CASAS_DECIMAIS_USUARIO) return ret.replace(".", ",");

			dec = Double.parseDouble(apos.substring(0, casas) + "." + apos.substring(casas));
			ret = antes + "," + Math.round(dec);
		}
		catch(Exception e){
			e.getMessage();
		}

		return ret;

	}

	public static String tabular(String x, int largCol){
		String str = "<span style='color:white'>";
		str += espaco(largCol - x.length());
		str += "</span><span style='color:black'>" + x + "</span>";
		return str ;
	}

	public static String td(String x){
		return "<td>" + x + "</td>";
	}

	public static String abreTr(){
		return "<tr>";
	}

	public static String fechaTr(){
		return "</tr>";
	}

}