package com.dunedin.sensorx.analise.utils;

import com.dunedin.sensorx.analise.core.Ponto;

import java.awt.Color;
import java.awt.geom.Ellipse2D;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.IOException;
import java.io.Serializable;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import java.text.DateFormat;
import java.text.MessageFormat;
import java.text.NumberFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.Random;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Scanner;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;

import org.jfree.chart.ChartFactory;

import org.jfree.chart.ChartUtils; //versao 1.6
//import org.jfree.chart.ChartUtilities; //versao 1.0.19
import org.jfree.chart.JFreeChart;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.general.DatasetUtils; 

import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.chart.renderer.xy.XYShapeRenderer;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.IntervalXYDataset;

import org.jfree.data.statistics.DefaultBoxAndWhiskerCategoryDataset;
import org.jfree.data.statistics.BoxAndWhiskerCategoryDataset;
import org.jfree.data.statistics.BoxAndWhiskerXYDataset;
import org.jfree.data.statistics.BoxAndWhiskerItem;
import org.jfree.data.statistics.HistogramDataset;

public class Grafico implements Constantes{
    
        public static String gerarRegressao(String data[][], double alfa, double beta, double loLimX, double upLimX) {

		JFreeChart grafico = null;
		XYSeriesCollection datasetPontos = new XYSeriesCollection();
		String nomeGraf = "";

		try {

			loLimX = (loLimX >= 0 ? 0 : loLimX);

			XYSeriesCollection datasetReta = new XYSeriesCollection();
	        XYSeries sReta = new XYSeries("Regressao");
	        sReta.add(loLimX, beta * loLimX + alfa);
	        sReta.add(upLimX, beta * upLimX + alfa);
	        datasetPontos.addSeries(sReta); 

	        XYSeries series = new XYSeries("Amostras");
	        for (int j=1; j < data[0].length; j++){
				series.add(Double.parseDouble(data[0][j]), Double.parseDouble(data[1][j]));
			}
			datasetPontos.addSeries(series);
		
			XYLineAndShapeRenderer r = new XYLineAndShapeRenderer();
			
	        r.setSeriesLinesVisible(0, true);
	        r.setSeriesShapesVisible(0, false);
	        r.setSeriesLinesVisible(1, false);
	        r.setSeriesShapesVisible(1, true);
	        
	        XYPlot plot = new XYPlot(datasetPontos, new NumberAxis(data[0][0]), new NumberAxis(data[1][0]), r);

	        grafico = new JFreeChart(" ", JFreeChart.DEFAULT_TITLE_FONT, plot, true);

	        nomeGraf = (new Date()).getTime() + ".png";

	        File file = new File(GRAFICOS_PATH + nomeGraf);

	        OutputStream out = new FileOutputStream(file);

	        ChartUtils.writeChartAsPNG(out, grafico, 500, 500, null);

	        out.close();

        }
        catch(Exception e){
        	e.printStackTrace();
        }

        return nomeGraf;

    }

    public static String gerarScatterPlot(double data[][], String tituloX, String tituloY) {

		JFreeChart grafico = null;
		XYSeriesCollection datasetPontos = new XYSeriesCollection();
		String nomeGraf = "";

		try {

			XYSeriesCollection datasetReta = new XYSeriesCollection();
	        
	        XYSeries series = new XYSeries("Amostras");
	        for (int j=0; j < data[0].length; j++){
				series.add(data[0][j], data[1][j]);
			}
			datasetPontos.addSeries(series);
		
			XYLineAndShapeRenderer r = new XYLineAndShapeRenderer();
			
	        r.setSeriesLinesVisible(0, false);
	        r.setSeriesShapesVisible(0, true);
	        
	        XYPlot plot = new XYPlot(datasetPontos, new NumberAxis(tituloX), new NumberAxis(tituloY), r);

	        grafico = new JFreeChart("Analise Componentes Principais ", JFreeChart.DEFAULT_TITLE_FONT, plot, true);

	        nomeGraf = (new Date()).getTime() + ".png";

	        File file = new File(GRAFICOS_PATH + nomeGraf);

	        OutputStream out = new FileOutputStream(file);

	        ChartUtils.writeChartAsPNG(out, grafico, 500, 500, null);

	        out.close();

        }
        catch(Exception e){
        	e.printStackTrace();
        }

        return nomeGraf;

    }
	
    public static String gerarMedias(JFreeChart grafico) {

    	String nomeImg = "";

    	try {

        	nomeImg = (new Date()).getTime() + ".png";
  
	        File file = new File(GRAFICOS_PATH + nomeImg);

	        OutputStream out = new FileOutputStream(file);

	        ChartUtils.writeChartAsPNG(out, grafico, 500, 500, null);

	        out.close();

        }
        catch(Exception ex){


        }

        return nomeImg;

    }

   
    public static String gerarHistograma(String titulo, double data[]) {

		JFreeChart grafico = null; 
		
		String nomeImg = "";
		
        //datasetHistograma.addSeries(1, data, data.length);
		//dataset = DatasetUtils.createCategoryDataset("A", "A", data);

        HistogramDataset dataset = new HistogramDataset();

        dataset.addSeries(0, data, data.length);

		grafico = ChartFactory.createHistogram("Histograma", titulo, "Frequencia",  dataset);

        //grafico =  ChartFactory.createLineChart(titulo, legendaX, legendaY, dataset);

        return gerarMedias(grafico);

    }

    public static String gerarBoxAndWhisker(String titulo, 
    										 double media[],
    										 double mediana[],
    										 double q1[],
    										 double q3[],
    										 double lInf[],
    										 double lSup[],
    										 double minExterior,
    										 double maxExterior) {

		JFreeChart grafico = null; 
		
		String nomeImg = "";
		
        //datasetHistograma.addSeries(1, data, data.length);
		//dataset = DatasetUtils.createCategoryDataset("A", "A", data);
		
		DefaultBoxAndWhiskerCategoryDataset dataset = new DefaultBoxAndWhiskerCategoryDataset();

		for (int i=0; i < media.length; i++){
			Comparable dum1 = new Dummy(i);
			Comparable dum2 = new Dummy(0);
			dataset.add(new BoxAndWhiskerItem(media[i], mediana[i], q1[i], q3[i], lInf[i], lSup[i], minExterior, maxExterior, null), dum1, dum2);
		}
		
		grafico = ChartFactory.createBoxAndWhiskerChart("Box Plot", titulo, " ", dataset, false);

        //grafico =  ChartFactory.createLineChart(titulo, legendaX, legendaY, dataset);

        return gerarMedias(grafico);

    }
    
    public static String gerarKMeans(Ponto[] data, Ponto[] aCentros, String tituloX, String tituloY){
        
        JFreeChart grafico = null;
        XYSeriesCollection datasetPontos = new XYSeriesCollection();
        String nomeGraf = "";
        Map<Integer, Color> colorMap = new HashMap<Integer, Color>();
        
        colorMap.put(0, Color.YELLOW);
        colorMap.put(1, Color.RED);
        colorMap.put(2, Color.GREEN);
        colorMap.put(3, Color.BLUE);
        colorMap.put(4, Color.GRAY);
        colorMap.put(5, Color.CYAN);
        colorMap.put(6, Color.BLACK);
        colorMap.put(7, Color.MAGENTA);

		try {

			//Ponto[] data = listaPonto.toArray(new Ponto[listaPonto.size()]);

			XYSeriesCollection datasetReta = new XYSeriesCollection();
	        
	        XYSeries series = new XYSeries("Cluster 0");
	        double clusterId    = 0.0;
	        double clusterIdOld = -1.0;
	        int cont = 0;
	        for (int j=0; j < data.length; j++){

	        	clusterId = data[j].getClusterId();
	        	//System.out.println(clusterId + ", " + clusterIdOld);
	        
	        	if (clusterId > clusterIdOld){
	        		if (clusterId > 0) datasetPontos.addSeries(series);
					series = new XYSeries("Cluster " + (int)clusterId);
					cont++;
	        	}

	        	series.add(data[j].getX(), data[j].getY());
				
				clusterIdOld = clusterId;

			}

        	if (clusterId > 0) datasetPontos.addSeries(series);
			cont++;

			XYSeries seriesCentros = new XYSeries("Centros");
	        for (int j=0; j < aCentros.length; j++){
				seriesCentros.add(aCentros[j].getX(), aCentros[j].getY());
		    }
			datasetPontos.addSeries(seriesCentros);
			cont++;
        	
			XYLineAndShapeRenderer r = new XYLineAndShapeRenderer();

			r.setDefaultShapesFilled(false); 
			r.setUseFillPaint(false);
			
			for (int i = 0; i < cont; i++){
		        r.setSeriesPaint(i, colorMap.get(i));
		    }
		    
		    for (int i = 0; i < cont; i++){
                        if (i < cont - 1)
                            r.setSeriesShape(i, new Ellipse2D.Double(-3, -3, 6, 6));
                        else
                            r.setSeriesShape(i, new Ellipse2D.Double(-3, -3, 50, 50));
                    }

		    for (int i = 0; i < cont; i++){
		        r.setSeriesShapesVisible(i, true);
		    }

		    for (int i = 0; i < cont; i++){
		        r.setSeriesLinesVisible(i, false);
	    	}
	        
	       
	        XYPlot plot = new XYPlot(datasetPontos, new NumberAxis(tituloX), new NumberAxis(tituloY), r);

	        grafico = new JFreeChart("Scatterplot", JFreeChart.DEFAULT_TITLE_FONT, plot, true);

	        nomeGraf = (new Date()).getTime() + ".png";

	        File file = new File(GRAFICOS_PATH + nomeGraf);

	        OutputStream out = new FileOutputStream(file);

	        ChartUtils.writeChartAsPNG(out, grafico, 500, 500, null);

	        out.close();

        }
        catch(Exception e){
        	e.printStackTrace();
        }

        return nomeGraf;   
        
    }

    /*
    private String gerarGraficoMediasAdq(String titulo, String legendaX, String legendaY, double data[][]) {

		JFreeChart grafico = null; 
		CategoryDataset dataset = null;
		String nomeImg = "";

        dataset = DatasetUtils.createCategoryDataset("A", "A", data);

        grafico =  ChartFactory.createLineChart(titulo, legendaX, legendaY, dataset);

        return gerarGraficoMedias(grafico);

    }

	private String gerarGraficoMediasAnovaTwoWay(double data[][]) {

		JFreeChart grafico = null; 
		CategoryDataset dataset = null;
		String nomeImg = "";

        dataset = DatasetUtils.createCategoryDataset("A", "A", data);

        grafico =  ChartFactory.createLineChart("Medias - ANOVA 2 Fatores", "Fator A", "Fator B", dataset);

        return gerarGraficoMedias(grafico);

    }
    
    private static IntervalXYDataset criarDatasetHistograma(double data[]) {
        HistogramDataset dataset = new HistogramDataset();
        dataset.addSeries(1, data, data.length);
        return (IntervalXYDataset) dataset;
    }
	*/

}