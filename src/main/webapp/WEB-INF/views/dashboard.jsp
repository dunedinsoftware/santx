<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ page isELIgnored="false"%>
<html>
<head>	
<title>API - Dunedin Software</title>

<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
<style>
@-webkit-keyframes spin {
  0% { -webkit-transform: rotate(0deg); }
  100% { -webkit-transform: rotate(360deg); }
}

@keyframes spin {
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
}

@-webkit-keyframes animatebottom {
  from { bottom:100px; opacity:0 } 
  to { bottom:0px; opacity:1 }
}

@keyframes animatebottom { 
  from{ bottom:100px; opacity:0 } 
  to{ bottom:0; opacity:1 }
}

body {
	background-color:white;
}

.btnStat{
	width:100%;
	height:9%;
	background:linear-gradient(lightgray, white);
	font-size:20px;	
}

.btnStatAlto{
	width:100%;
	height:10%;
	background:linear-gradient(lightgray, white);
	font-size:20px;	
}

.btnAdm{
	width:100%;
	height:calc(17% - 20px);
	background:linear-gradient(lightgray, white);
	font-size:25px;
}

#menuEsq {
	float:left;
	width:10%;
        height: 90%;
}

#btnHome {
	border-radius:25px 0px 0px 0px;
	border: solid 1px white;
	width:100%;
	height:100px;
	background-color:white;
}

#btnHome span:nth-child(1) {
	font-size:36px;
	font-family:Tahoma;
	font-style:italic;
	color:black;
}

#btnHome span:nth-child(2) {
	font-size:36px;
	font-family:Tahoma;
	font-style:italic;
	color:red;
}

#btnHome span:nth-child(3) {
	font-size:36px;
	font-family:Tahoma;
	font-style:italic;
	color:blue;
}

#btnHome span:nth-child(4) {
	font-size:36px;
	font-family:Tahoma;
	font-style:italic;
	color:red;
}

#btnHome span:nth-child(5) {
	float:left;
	margin-left:20%;
	font-size:10px;
	font-family:Tahoma;
	font-style:normal;
	color:black;
}

#btnCantoEsqInf {
	border-radius:0px 0px 0px 25px;
	width:100%;
	height:4%;
	background-color:lightgray;
}

#areaTrabalho{
	float:left;
	width:80%;
	background-color:white;	
}

#areaTrabalhoArquivos{
	float:left;
	width:100%;
	height:100px;
	overflow:auto;
	background-color:lightgray;
}

#areaTrabalhoLog{
	float:left;
	top:100px;
	width:100%;
	height:600px;
	overflow:auto;
	font-family:Tahoma;
	padding: 5px;
}

#areaTrabalhoAmostras1{
	visibility:hidden;
	position:absolute;
	float:left;
	top:108px;
	/* left:calc(10% + 5px); */
	left:calc(90% - 5px);
	height:300px;
	font-family:Tahoma;
	border: 2px solid gray;
	box-shadow:3px 3px;
	overflow:auto;
	background-color:#E5E5E5;
	z-index:5;
}

#areaTrabalhoAmostras1 span:nth-child(1) {
	position:relative;
	float:right;
	top:2px;
	margin-right:2px;
	width:15px;
	height:15px;
	padding:1px;
	border:1px solid black;
}

#btnStatusExibicao1  span:nth-child(1){
	margin-left:2px;
	font-family:sans-serif;
	font-size:16px;
	color:black;
}


#menuDir {
	float:left;
	width:10%;
}

#btnCantoDirSup {
	border-radius:0px 25px 0px 0px;
	width:100%;
	height:100px;
	background:lightgray;
}

#btnCantoDirInf{
	border-radius:0px 0px 25px 0px;
	width:100%;
	height:calc(16% - 5px);
	background-color:#E5E5E5;
	font-size:15px;
	font-family:Tahoma;
}

/* Caixa de dialogo */

.dialogo {
	visibility:hidden;
	position:absolute;
	top:250px;
	left:350px;
	width:600px;
	height:100px;
	border:1px solid;
	background-color:#E5E5E5;
	z-index:10;
	box-shadow:3px 3px;
}

.dialogo div:nth-child(1){
	height:24px;
	font-family:Tahoma;
}



.dialogo div:nth-child(1) div:nth-child(1){ 
	float:left;
	width:90%;
	text-align:center;
	color:white;
	font-family: Tahoma;
}

.dialogo div:nth-child(1) div:nth-child(2){ 
	position:absolute;
	margin-top:2px;
	margin-left: calc(100% - 20px);
	width:12px;
	height:17px;
	padding-left: 7px;
	background-color: white;
}

.dialogo div:nth-child(1) div:nth-child(2) a {

}

.dialogo div:nth-child(1) div:nth-child(2) a span { 
	margin-top:-3px;
	color:black;
	cursor:hand;
	font-family:sans-serif;
	font-size:15px;
}

.dialogo div:nth-child(2){
	overflow: auto;
	font-family: Tahoma;
}

/************************ LOADER ******************************/
#loader {
  position: absolute;
  left: 50%;
  top: 50%;
  z-index: 2;
  width: 150px;
  height: 150px;
  margin: -75px 0 0 -75px;
  border: 16px solid red;
  border-radius: 50%;
  border-top: 16px solid black;
  width: 120px;
  height: 120px;
  -webkit-animation: spin 2s linear infinite;
  animation: spin 2s linear infinite;
}

.animate-bottom {
  position: relative;
  -webkit-animation-name: animatebottom;
  -webkit-animation-duration: 1s;
  animation-name: animatebottom;
  animation-duration: 1s;
}      
</style>
</head>
<body>
<div>
	<div id="menuEsq">
		<button id="btnHome"><span>Sa</span><span></span><span>n</span><span>tX</span><span>VERS&Atilde;O STANDARD</span></button>
		<!--<button id="btnADQ" class="btnStat">ADQ PARCIAL</button>-->
		<div style="overflow-y:scroll;overflow-x:hidden">
                    <button id="btnHistograma" class="btnStat">HISTOGRAMA</button>
                    <button id="btnTwoWayAnova" class="btnStatAlto">ANOVA 2 FATORES</button>	
                    <button id="btnOneWayAnova" class="btnStatAlto">ANOVA 1 FATOR</button>	
                    <button id="btnOneWayAnovaBca" class="btnStatAlto">ANOVA 1 FATOR BCA</button>
                    <button id="btnTukey" class="btnStat" >TUKEY</button>
                    <button id="btnRegressao" class="btnStat" >REGRESS&AtildeO</button>
                    <button id="btnCorrelacao" class="btnStat" >CORRELA&Ccedil;&AtildeO</button>
                    <button id="btnFriedman" class="btnStat" >FRIEDMAN</button>
                    <button id="btnCochran" class="btnStat">COCHRAN</button>
                    <button id="btnKmeans" class="btnStat">K-MEANS</button>
		</div>
		<button id="btnCantoEsqInf"></button>	
	</div>
	<div id="areaTrabalho">
		<div id="loader" style="display:none"></div>
		<div id="underloader" class="animate-bottom">
			<div id="areaTrabalhoArquivos"></div>
			<div id="areaTrabalhoLog" ></div>
		</div>
	</div>
	<div id="menuDir">
		<button id="btnCantoDirSup"></button>
		<button id="btnEditar" class="btnAdm">Editar</button>
		<button id="btnImportar" class="btnAdm">Importar</button>
		<button id="btnExportar" class="btnAdm">Exportar</button>
		<button id="btnLimpar" class="btnAdm">Limpar</button>
		<button id="btnAjuda" class="btnAdm">Ajuda</button>
		<button id="btnCantoDirInf">Dunedin Software</button>
	</div>
</div>
<!-- Flutuante -->
<div id="areaTrabalhoAmostras1" class="dialogo" style="position: fixed">
	<div style="background-color:red;position:fixed">
		<div></div>
		<div><a id="btnStatusExibicao1"><span>X</span></a></div>
	</div>
	<div style="margin-top:24px;"></div>
</div>

<!-- Caixa de Dialogo -->
<div id="dialogoUpload" class="dialogo">
	<div style="background-color:blue">
		<div><span>Importar</span></div>
		<div><a id="btnFecharDialogo"><span>X</span></a></div>
	</div>
	<div style="padding-left: 10px;"></br>
		Escolha o arquivo e clique Enviar
		<form id="frmUpload" enctype="multipart/form-data" action="http://vps27371.publiccloud.com.br:8080/SantX/stat/upload">
			<input id="arquivo" type="file" name="arquivo" />
			<input id="btnEnviarArquivo" type="button" value="Enviar" />
		</form>
	</div>
</div>
<!-- Caixa de Dialogo -->
<div id="dialogoTwoWayAnova" class="dialogo">
	<div style="background-color:blue">
		<div><span>ANOVA 2 Fatores</span></div>
		<div><a id="btnFecharDialogo"><span>X</span></a></div>
	</div>
	<div style="padding-left: 10px;"></br>
		<form>
		<span>Com Fator B:</span><select id="selArqCT"></select>
		<span>Sem Fator B:</span><select id="selArqST"></select>
		<button id="btnExecTwoWayAnova" type="button">OK</button>
		</form>
	</div>
</div>
<!-- Caixa de Dialogo -->
<div id="dialogo" class="dialogo">
	<div>
		<div></div>
		<div><a id="btnFecharDialogo"><span>X</span></a></div>
	</div>
	<div></div>
</div>
<script>
 
/*\
|*|
|*|  :: XMLHttpRequest.prototype.sendAsBinary() Polyfill ::
|*|
|*|  https://developer.mozilla.org/en-US/docs/DOM/XMLHttpRequest#sendAsBinary()
\*/

if (!XMLHttpRequest.prototype.sendAsBinary) {
  XMLHttpRequest.prototype.sendAsBinary = function(sData) {
    var nBytes = sData.length, ui8Data = new Uint8Array(nBytes);
    for (var nIdx = 0; nIdx < nBytes; nIdx++) {
      ui8Data[nIdx] = sData.charCodeAt(nIdx) & 0xff;
    }
    /* send as ArrayBufferView...: */
    this.send(ui8Data);
    /* ...or as ArrayBuffer (legacy)...: */
    //this.send(ui8Data.buffer); 
  };
}

/*\
|*|
|*|  :: AJAX Form Submit Framework ::
|*|
|*|  https://developer.mozilla.org/en-US/docs/DOM/XMLHttpRequest/Using_XMLHttpRequest
|*|
|*|  This framework is released under the GNU Public License, version 3 or later.
|*|  https://www.gnu.org/licenses/gpl-3.0-standalone.html
|*|
|*|  Syntax:
|*|
|*|   AJAXSubmit(HTMLFormElement);
\*/

var AJAXSubmit = (function (){

  function ajaxSuccess () {
    /* console.log("AJAXSubmit - Success!"); */
    console.log(this.responseText);
    /* you can get the serialized data through the "submittedData" custom property: */
    /* console.log(JSON.stringify(this.submittedData)); */
  }

  function submitData (oData) {
    /* the AJAX request... */
    var oAjaxReq = new XMLHttpRequest();
    oAjaxReq.submittedData = oData;
    oAjaxReq.onload = ajaxSuccess;
    
      /* method is POST */
      oAjaxReq.open("post", oData.receiver, true);
      
        /* enctype is multipart/form-data */
        var sBoundary = "---------------------------" + Date.now().toString(16);
        oAjaxReq.setRequestHeader("Content-Type", "multipart\/form-data; boundary=" + sBoundary);
        oAjaxReq.sendAsBinary("--" + sBoundary + "\r\n" +
            oData.segments.join("--" + sBoundary + "\r\n") + "--" + sBoundary + "--\r\n");
      
    
  }

  function processStatus (oData) {
    if (oData.status > 0) { return; }
    /* the form is now totally serialized! do something before sending it to the server... */
    /* doSomething(oData); */
    /* console.log("AJAXSubmit - The form is now serialized. Submitting..."); */
    submitData (oData);
  }

  function pushSegment (oFREvt) {
    this.owner.segments[this.segmentIdx] += oFREvt.target.result + "\r\n";
    this.owner.status--;
    processStatus(this.owner);
  }

  function plainEscape (sText) {
    /* How should I treat a text/plain form encoding?
       What characters are not allowed? this is what I suppose...: */
    /* "4\3\7 - Einstein said E=mc2" ----> "4\\3\\7\ -\ Einstein\ said\ E\=mc2" */
    return sText.replace(/[\s\=\\]/g, "\$&");
  }

  function SubmitRequest (oTarget) {
    var nFile, sFieldType, oField, oSegmReq, oFile, bIsPost = oTarget.method.toLowerCase() === "post";
    /* console.log("AJAXSubmit - Serializing form..."); */
    this.contentType = bIsPost && oTarget.enctype ? oTarget.enctype : "application\/x-www-form-urlencoded";
    this.technique = 3;
    this.receiver = oTarget.action;
    this.status = 0;
    this.segments = [];
    var fFilter = this.technique === 2 ? plainEscape : escape;
    for (var nItem = 0; nItem < oTarget.elements.length; nItem++) {
      oField = oTarget.elements[nItem];
      if (!oField.hasAttribute("name")) { continue; }
      sFieldType = oField.nodeName.toUpperCase() === "INPUT" ? oField.getAttribute("type").toUpperCase() : "TEXT";
      if (sFieldType === "FILE" && oField.files.length > 0) {
        if (this.technique === 3) {
          /* enctype is multipart/form-data */
          for (nFile = 0; nFile < oField.files.length; nFile++) {
            oFile = oField.files[nFile];
            oSegmReq = new FileReader();
            /* (custom properties:) */
            oSegmReq.segmentIdx = this.segments.length;
            oSegmReq.owner = this;
            /* (end of custom properties) */
            oSegmReq.onload = pushSegment;
            this.segments.push("Content-Disposition: form-data; name=\"" +
                oField.name + "\"; filename=\"" + oFile.name +
                "\"\r\nContent-Type: " + oFile.type + "\r\n\r\n");
            this.status++;
            oSegmReq.readAsBinaryString(oFile);
          }
        } else {
          /* enctype is application/x-www-form-urlencoded or text/plain or
             method is GET: files will not be sent! */
          for (nFile = 0; nFile < oField.files.length;
              this.segments.push(fFilter(oField.name) + "=" + fFilter(oField.files[nFile++].name)));
        }
      } else if ((sFieldType !== "RADIO" && sFieldType !== "CHECKBOX") || oField.checked) {
        /* NOTE: this will submit _all_ submit buttons. Detecting the correct one is non-trivial. */
        /* field type is not FILE or is FILE but is empty */
        this.segments.push(
          this.technique === 3 ? /* enctype is multipart/form-data */
            "Content-Disposition: form-data; name=\"" + oField.name + "\"\r\n\r\n" + oField.value + "\r\n"
          : /* enctype is application/x-www-form-urlencoded or text/plain or method is GET */
            fFilter(oField.name) + "=" + fFilter(oField.value)
        );
      }
    }
    processStatus(this);
  }

  return function (oFormElement) {
    if (!oFormElement.action) { return; }
    new SubmitRequest(oFormElement);
  };

})();

$("#btnEnviarArquivo").on("click", function(){

  AJAXSubmit(document.getElementById("frmUpload"));

  $.get(context + "stat/arquivos", function(responseText){

        var resp = JSON.parse(responseText);
        var ret = "";
        var arqs = resp[0];

        var path = $("#arquivo").val();
        var arqUploadComExt = path.substring(path.lastIndexOf("\\") + 1);
        var arqUpload = arqUploadComExt.substring(0, arqUploadComExt.indexOf("."));
  
    
        $.each(arqs.arquivos, function(index, arq) {
          ret += "<button type=button name='" + arq + "' onclick=showAmostras('" + arq + "') style='height:50px;font-size:20px;border-radius:5px'>"+ arq + "</button>";
        });
        ret += "<button type=button name='" + arqUpload + "' onclick=showAmostras('" + arqUpload + "') style='height:50px;font-size:20px;border-radius:5px'>"+ arqUpload + "</button>";


        $("#areaTrabalhoArquivos").html(ret);

  }, "html");
  
  $("#dialogoUpload").css("visibility", "hidden");

});   
</script>
<script>
var domain = "http://vps27371.publiccloud.com.br:8080";
//var domain = "http://dragon.kinghost.net";
var context = domain + "/SantX/";
var sep = "</br></br>";

function disableLoader(){
	$("#loader").css("display", "none");
}

function enableLoader(){
	$("#loader").css("display", "block");
}

$("#btnStatusExibicao1").on("click", function(){

	$("#areaTrabalhoAmostras1").css("visibility", "hidden");
	
});

$("#btnStatusExibicao2").on("click", function(){

	$("#areaTrabalhoAmostras2").css("visibility", "hidden");
	
});

$("#btnLimpar").on("click", function(){

	$("#areaTrabalhoLog").html("");
	
});

$("#btnHistograma").on("click", function(){

	var url = ""; 
	url += context + "stat/histograma";
	url += "?nomeArq=" + $("#tabelaAmostras").attr("amostras-nome");

	enableLoader();

	$.get(url, function(responseText){
    
    	var htm = $("#areaTrabalhoLog").html();
    
    	$("#areaTrabalhoLog").html(sep + responseText + htm);

    	disableLoader();
	    		
	}, "html");	
	
});


/*
$("#btnADQ").on("click", function(){

	enableLoader();

	var url = ""; 
	url += context + "stat/adq";
	url += "?nomeArq=" + $("#tabelaAmostras").attr("amostras-nome");

	$.get(url, function(responseText){
    
    	var htm = $("#areaTrabalhoLog").html();
    
    	$("#areaTrabalhoLog").html(sep + responseText + htm);

    	disableLoader();
	    		
	}, "html");	
	
});
*/

$("#btnFriedman").on("click", function(){

	var url = ""; 
	url += context + "stat/friedman";
	url += "?nomeArq=" + $("#tabelaAmostras").attr("amostras-nome");

	$.get(url, function(responseText){
    
    	var htm = $("#areaTrabalhoLog").html();
    
    	$("#areaTrabalhoLog").html(sep + responseText + htm);
	    		
	}, "html");	
	
});

$("#btnTukey").on("click", function(){

	var url = ""; 
	url += context + "stat/tukey";
	url += "?nomeArq=" + $("#tabelaAmostras").attr("amostras-nome");

	$.get(url, function(responseText){
    
    	var htm = $("#areaTrabalhoLog").html();
    
    	$("#areaTrabalhoLog").html(sep + responseText + htm);
	    		
	}, "html");	
	
});

$("#btnCochran").on("click", function(){

	var url = ""; 
	url += context + "stat/cochran";
	url += "?nomeArq=" + $("#tabelaAmostras").attr("amostras-nome");

	$.get(url, function(responseText){
    
    	var htm = $("#areaTrabalhoLog").html();
    
    	$("#areaTrabalhoLog").html(sep + responseText + htm);
	    		
	}, "html");	
	
});

$("#btnOneWayAnovaBca").on("click", function(){

	var url = ""; 
	url += context + "stat/anova-bca";
	url += "?nomeArq=" + $("#tabelaAmostras").attr("amostras-nome");

	$.get(url, function(responseText){
    
    	var htm = $("#areaTrabalhoLog").html();
    
    	$("#areaTrabalhoLog").html(sep + responseText + htm);
	    		
	}, "html");	
	
});

$("#btnOneWayAnova").on("click", function(){

	var url = ""; 
	url += context + "stat/anova";
	url += "?nomeArq=" + $("#tabelaAmostras").attr("amostras-nome");

	$.get(url, function(responseText){

		var htm = $("#areaTrabalhoLog").html();
    
    	$("#areaTrabalhoLog").html(sep + responseText + htm);
	    		
	}, "html");	
	
});

$("#btnRegressao").on("click", function(){

	var url = ""; 
	url += context + "stat/regressao";
	url += "?nomeArq=" + $("#tabelaAmostras").attr("amostras-nome");

	$.get(url, function(responseText){

		var htm = $("#areaTrabalhoLog").html();
    
    	$("#areaTrabalhoLog").html(sep + responseText + htm);
	    		
	}, "html");	
	
});

$("#btnCorrelacao").on("click", function(){

	var url = ""; 
	url += context + "stat/correlacao";
	url += "?nomeArq=" + $("#tabelaAmostras").attr("amostras-nome");

	$.get(url, function(responseText){

		var htm = $("#areaTrabalhoLog").html();
    
    	$("#areaTrabalhoLog").html(sep + responseText + htm);
	    		
	}, "html");	
	
});

$("#btnKmeans").on("click", function(){

	var url = ""; 
	url += context + "stat/k-means";
	url += "?nomeArq=" + $("#tabelaAmostras").attr("amostras-nome");

	$.get(url, function(responseText){

		var htm = $("#areaTrabalhoLog").html();
    
    	$("#areaTrabalhoLog").html(sep + responseText + htm);
	    		
	}, "html");	
	
});

$("#btnImportar").on("click", function(){

	$("#dialogoUpload").css("visibility", "visible");	
	
});

$("#dialogoUpload a").on("click", function(){

	$("#dialogoUpload").css("visibility", "hidden");	
	
});

$("#btnTwoWayAnova").on("click", function(){

	var arquivos = $("#areaTrabalhoArquivos button");

	$("#selArqCT").empty();
	$("#selArqST").empty();


	$("#selArqCT").append($("<option />").val(0).text("Selecione a amostra"));
	$.each(arquivos, function(index, arquivo) {
		$("#selArqCT").append($("<option />").val(arquivo.name).text(arquivo.name));
	});

	$("#selArqST").append($("<option />").val(0).text("Selecione a amostra"));
	$.each(arquivos, function(index, arquivo) {
		$("#selArqST").append($("<option />").val(arquivo.name).text(arquivo.name));
	});	

	$("#dialogoTwoWayAnova").css("visibility", "visible");	
	
});

$("#dialogoTwoWayAnova a").on("click", function(){

	$("#dialogoTwoWayAnova").css("visibility", "hidden");	
	
});

$("#dialogo a").on("click", function(){

	$("#dialogo").css("visibility", "hidden");	
	
});

$("#btnExecTwoWayAnova").on("click", function(){

	var url = ""; 
	url += context + "stat/anova-two-way";
	url += "?nomeArqCT=" + $("#selArqCT option:selected").text();
	url += "&nomeArqST=" + $("#selArqST option:selected").text();

	$.get(url, function(responseText){
    
    	var htm = $("#areaTrabalhoLog").html();
    
    	$("#areaTrabalhoLog").html(sep + responseText + htm);
    	$("#areaTrabalhoAmostras1").css("visibility", "visible");
	    		
	}, "html");	

	$("#dialogoTwoWayAnova").css("visibility", "hidden");


	
});

var showAviso = function(){

	$("#dialogo").css("visibility", "visible");

};

var showAmostras = function(nomeArq){

	$.get(context + "stat/arquivos/" + nomeArq,
	 function(responseText){
	    
	    	var amostras = amostras2 = JSON.parse(responseText);
	    	var htm = "";
	    	var matriz = "";
	    	var i=0;
	    	var j=0;

	    	htm += "<table id='tabelaAmostras' amostras-nome='" + nomeArq + "'";
	    	htm += " border=0>";
	    	htm += "<tr style='position:fixed;background-color:#E5E5E5'>";

			$.each(amostras, function(index, amostra) {
				var fator = amostra.fator;
				var notas = amostra.notas;
				htm += "<td width='100px' align=center>" + fator + "</td>";
				$.each(notas, function(index, nota) {
					j++;
				});

				i++;
			});
			htm += "</tr>";

			matriz = new Array(i);
	
			for (var k=0; k < matriz.length ; k++){
				matriz[k] = new Array(Math.round(j / i));
			} 

			i = 0; j = 0;
			$.each(amostras, function(index, amostra) {
				var notas = amostra.notas;
				$.each(notas, function(index, nota) {
					//console.log(nota);
					matriz[i][j++] = nota;	
				});
				j = 0;
				i++;
			});
			
			var width = matriz.length * 100 + 25;

			for (var j=0; j < matriz[0].length ; j++){
				if (j % 2 > 0)
					htm += "<tr bgcolor='white'>";
				else
					htm += "<tr>";
				for (var i=0; i < matriz.length ; i++){
					htm += "<td width='100px' align=right>" + matriz[i][j] + "</td>";
				}
				htm += "</tr>";
			}

			htm += "</table>";
			
			var left  = "calc(90% - 12px - " + width + "px)";

			$("#areaTrabalhoAmostras1 div:nth-child(1)").css("width", width + "px");
			$("#areaTrabalhoAmostras1 div:nth-child(1) div:nth-child(1)").html(nomeArq);
			$("#areaTrabalhoAmostras1 > div:nth-child(2)").html(htm);
			$("#areaTrabalhoAmostras1").css("width", width + "px");
			$("#areaTrabalhoAmostras1").css("left", left );
			$("#areaTrabalhoAmostras1").css("visibility", "visible");
		
	}, "html");	

};

$(function() {

	$.get(context + "stat/arquivos", function(responseText){
      
        var resp = JSON.parse(responseText);

        var arqs = resp[0];

        var ret = "";

        $.each(arqs.arquivos, function(index, arq) {
        ret += "<button type=button' name='" + arq + "' onclick=showAmostras('" + arq + "') style='height:50px;font-size:20px;border-radius:5px'>"+ arq + "</button>";
        });
        ret += "";
      
      $("#areaTrabalhoArquivos").html(ret);

  }, "html");

});
</script>
</body>
</html>