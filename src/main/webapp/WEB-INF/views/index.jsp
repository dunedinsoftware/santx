<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page isELIgnored="false"%>
<html lang="pt-br">
<head>
	<title>Login</title>
</head>
<body style="font-family:Verdana;font-size:1.5em;width:100%;height:3000px">

<div class="container">
  <form id="frmSignin" name="logonForm" method="POST" action="${pageContext.request.contextPath}/autenticar" class="form-signin">
	<h3 class="form-signin-heading" style="color:white">DUNEDIN SOFTWARE | WHITE-LABEL API</h3>
	<p style="color:orange;font-size:1em" align="justify">Seja bem-vind(o)a<p>
	<input type="text" name="username" id="username" class="form-control" placeholder="E-mail" />
	<input type="password" name="password" id="password" class="form-control" maxlength="8" placeholder="Senha de 8 digitos"/></br>
        <input type="submit" class="btn btn-lg btn-success btn-block" value="Entrar">
  </form></br>
  </br></br>
  </div>
</div> <!-- /container -->

</body>
</html>