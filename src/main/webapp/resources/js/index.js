var domain = "http://127.0.0.1:8080";
//var domain = "http://dragon.kinghost.net";
var context = domain + "/SensorXServer/";
var sep = "</br></br>";

function disableLoader(){
	$("#loader").css("display", "none");
}

function enableLoader(){
	$("#loader").css("display", "block");
}

$("#btnStatusExibicao1").on("click", function(){

	$("#areaTrabalhoAmostras1").css("visibility", "hidden");
	
});

$("#btnStatusExibicao2").on("click", function(){

	$("#areaTrabalhoAmostras2").css("visibility", "hidden");
	
});

$("#btnLimpar").on("click", function(){

	$("#areaTrabalhoLog").html("");
	
});

$("#btnHistograma").on("click", function(){

	var url = ""; 
	url += context + "stat/histograma";
	url += "?nomeArq=" + $("#tabelaAmostras").attr("amostras-nome");

	enableLoader();

	$.get(url, function(responseText){
    
    	var htm = $("#areaTrabalhoLog").html();
    
    	$("#areaTrabalhoLog").html(sep + responseText + htm);

    	disableLoader();
	    		
	}, "html");	
	
});


/*
$("#btnADQ").on("click", function(){

	enableLoader();

	var url = ""; 
	url += context + "stat/adq";
	url += "?nomeArq=" + $("#tabelaAmostras").attr("amostras-nome");

	$.get(url, function(responseText){
    
    	var htm = $("#areaTrabalhoLog").html();
    
    	$("#areaTrabalhoLog").html(sep + responseText + htm);

    	disableLoader();
	    		
	}, "html");	
	
});
*/

$("#btnFriedman").on("click", function(){

	var url = ""; 
	url += context + "stat/friedman";
	url += "?nomeArq=" + $("#tabelaAmostras").attr("amostras-nome");

	$.get(url, function(responseText){
    
    	var htm = $("#areaTrabalhoLog").html();
    
    	$("#areaTrabalhoLog").html(sep + responseText + htm);
	    		
	}, "html");	
	
});

$("#btnTukey").on("click", function(){

	var url = ""; 
	url += context + "stat/tukey";
	url += "?nomeArq=" + $("#tabelaAmostras").attr("amostras-nome");

	$.get(url, function(responseText){
    
    	var htm = $("#areaTrabalhoLog").html();
    
    	$("#areaTrabalhoLog").html(sep + responseText + htm);
	    		
	}, "html");	
	
});

$("#btnCochran").on("click", function(){

	var url = ""; 
	url += context + "stat/cochran";
	url += "?nomeArq=" + $("#tabelaAmostras").attr("amostras-nome");

	$.get(url, function(responseText){
    
    	var htm = $("#areaTrabalhoLog").html();
    
    	$("#areaTrabalhoLog").html(sep + responseText + htm);
	    		
	}, "html");	
	
});

$("#btnOneWayAnovaBca").on("click", function(){

	var url = ""; 
	url += context + "stat/anova-bca";
	url += "?nomeArq=" + $("#tabelaAmostras").attr("amostras-nome");

	$.get(url, function(responseText){
    
    	var htm = $("#areaTrabalhoLog").html();
    
    	$("#areaTrabalhoLog").html(sep + responseText + htm);
	    		
	}, "html");	
	
});

$("#btnOneWayAnova").on("click", function(){

	var url = ""; 
	url += context + "stat/anova";
	url += "?nomeArq=" + $("#tabelaAmostras").attr("amostras-nome");

	$.get(url, function(responseText){

		var htm = $("#areaTrabalhoLog").html();
    
    	$("#areaTrabalhoLog").html(sep + responseText + htm);
	    		
	}, "html");	
	
});

$("#btnRegressao").on("click", function(){

	var url = ""; 
	url += context + "stat/regressao";
	url += "?nomeArq=" + $("#tabelaAmostras").attr("amostras-nome");

	$.get(url, function(responseText){

		var htm = $("#areaTrabalhoLog").html();
    
    	$("#areaTrabalhoLog").html(sep + responseText + htm);
	    		
	}, "html");	
	
});

$("#btnCorrelacao").on("click", function(){

	var url = ""; 
	url += context + "stat/correlacao";
	url += "?nomeArq=" + $("#tabelaAmostras").attr("amostras-nome");

	$.get(url, function(responseText){

		var htm = $("#areaTrabalhoLog").html();
    
    	$("#areaTrabalhoLog").html(sep + responseText + htm);
	    		
	}, "html");	
	
});

$("#btnImportar").on("click", function(){

	$("#dialogoUpload").css("visibility", "visible");	
	
});

$("#dialogoUpload a").on("click", function(){

	$("#dialogoUpload").css("visibility", "hidden");	
	
});

$("#btnTwoWayAnova").on("click", function(){

	var arquivos = $("#areaTrabalhoArquivos button");

	$("#selArqCT").empty();
	$("#selArqST").empty();


	$("#selArqCT").append($("<option />").val(0).text("Selecione a amostra"));
	$.each(arquivos, function(index, arquivo) {
		$("#selArqCT").append($("<option />").val(arquivo.name).text(arquivo.name));
	});

	$("#selArqST").append($("<option />").val(0).text("Selecione a amostra"));
	$.each(arquivos, function(index, arquivo) {
		$("#selArqST").append($("<option />").val(arquivo.name).text(arquivo.name));
	});	

	$("#dialogoTwoWayAnova").css("visibility", "visible");	
	
});

$("#dialogoTwoWayAnova a").on("click", function(){

	$("#dialogoTwoWayAnova").css("visibility", "hidden");	
	
});

$("#dialogo a").on("click", function(){

	$("#dialogo").css("visibility", "hidden");	
	
});

$("#btnExecTwoWayAnova").on("click", function(){

	var url = ""; 
	url += context + "stat/anova-two-way";
	url += "?nomeArqCT=" + $("#selArqCT option:selected").text();
	url += "&nomeArqST=" + $("#selArqST option:selected").text();

	$.get(url, function(responseText){
    
    	var htm = $("#areaTrabalhoLog").html();
    
    	$("#areaTrabalhoLog").html(sep + responseText + htm);
    	$("#areaTrabalhoAmostras1").css("visibility", "visible");
	    		
	}, "html");	

	$("#dialogoTwoWayAnova").css("visibility", "hidden");


	
});

var showAviso = function(){

	$("#dialogo").css("visibility", "visible");

}

var showAmostras = function(nomeArq){

	$.get(context + "stat/arquivos/" + nomeArq,
	 function(responseText){
	    
	    	var amostras = amostras2 = JSON.parse(responseText);
	    	var htm = "";
	    	var matriz = "";
	    	var i=0;
	    	var j=0;

	    	htm += "<table id='tabelaAmostras' amostras-nome='" + nomeArq + "'";
	    	htm += " border=0>";
	    	htm += "<tr style='position:fixed;background-color:#E5E5E5'>";

			$.each(amostras, function(index, amostra) {
				var fator = amostra.fator;
				var notas = amostra.notas;
				htm += "<td width='100px' align=center>" + fator + "</td>";
				$.each(notas, function(index, nota) {
					j++;
				});

				i++;
			});
			htm += "</tr>";

			matriz = new Array(i);
	
			for (var k=0; k < matriz.length ; k++){
				matriz[k] = new Array(Math.round(j / i));
			} 

			i = 0; j = 0;
			$.each(amostras, function(index, amostra) {
				var notas = amostra.notas;
				$.each(notas, function(index, nota) {
					//console.log(nota);
					matriz[i][j++] = nota;	
				});
				j = 0;
				i++;
			});
			
			var width = matriz.length * 100 + 25;

			for (var j=0; j < matriz[0].length ; j++){
				if (j % 2 > 0)
					htm += "<tr bgcolor='white'>";
				else
					htm += "<tr>";
				for (var i=0; i < matriz.length ; i++){
					htm += "<td width='100px' align=right>" + matriz[i][j] + "</td>";
				}
				htm += "</tr>";
			}

			htm += "</table>";
			
			var left  = "calc(90% - 12px - " + width + "px)";

			$("#areaTrabalhoAmostras1 div:nth-child(1)").css("width", width + "px");
			$("#areaTrabalhoAmostras1 div:nth-child(1) div:nth-child(1)").html(nomeArq);
			$("#areaTrabalhoAmostras1 > div:nth-child(2)").html(htm);
			$("#areaTrabalhoAmostras1").css("width", width + "px");
			$("#areaTrabalhoAmostras1").css("left", left );
			$("#areaTrabalhoAmostras1").css("visibility", "visible");
		
	}, "html");	

}

$(function() {

	$.get(context + "stat/arquivos", function(responseText){
      
        var resp = JSON.parse(responseText);

        var arqs = resp[0];

        var ret = "";

        $.each(arqs.arquivos, function(index, arq) {
        ret += "<button type=button' name='" + arq + "' onclick=showAmostras('" + arq + "') style='height:50px;font-size:20px;border-radius:5px'>"+ arq + "</button>";
        });
        ret += "";
      
      $("#areaTrabalhoArquivos").html(ret);

  }, "html");

})
